set -x
set -e

_SIM=../../Release/lte-sim
#_SIM=../../Debug/lte-sim
_COUNT=0
_NB_SEEDS=30
_TRACE_DIR=TRACE-DW

if [ ! -d ${_TRACE_DIR} ]; then
    mkdir -p $_TRACE_DIR
fi

until [ $_COUNT -gt $_NB_SEEDS ]; do
for weight in 0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 0.95 1; do
trace=${_TRACE_DIR}/SCHED_${weight}_${_COUNT}

if [ ! -f $trace ]; then
    time $_SIM M2M-DW $weight $_COUNT > $trace
fi

done
_COUNT=$(($_COUNT + 1))
done
