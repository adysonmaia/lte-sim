set -x
set -e

_SIM=../../Release/lte-sim
#_SIM=../../Debug/lte-sim
_COUNT=0
_NB_SEEDS=30
_NB_VOIP=30
_NB_VIDEO=30
_NB_CBR=30

until [ $_COUNT -gt $_NB_SEEDS ]; do
for ue in 0 200 400 600 800 1000; do #number of users
#for sched in 6 8 10 12 13 15 16 18 19 20; do #scheduling algorithm
for sched in 19; do

ue_time=$(( (7 * $ue) / 10 )) # 70 %
ue_event=$(($ue - $ue_time))
trace=TRACE/SCHED_${sched}_UE_${ue}_${_COUNT}

if [ ! -f $trace ]; then
    time $_SIM M2M $sched $_NB_VOIP $_NB_VIDEO $_NB_CBR $ue_time $ue_event $_COUNT > $trace
fi

done
done
_COUNT=$(($_COUNT + 1))
done
