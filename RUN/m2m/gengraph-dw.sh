#!/bin/bash
#set -x

rm -f GNUPLOT/*
rm -f IMG/*

for file in "PLR" "TPUT" "FAIR" "PW"; do
title=""
if [ $file == "PLR" ]; then
title="Packets not meeting delay constraint (%)"
elif [ $file == "TPUT" ]; then
title="Throughput [mbps]"
elif [ $file == "FAIR" ]; then
title="Fairness Index"
elif [ $file == "PW" ]; then
title="Power Efficiency [byte/dBm]"
fi

for type in VOIP VIDEO CBR M2M_TIME M2M_EVENT M2M H2H; do
fileIn="CSV-DW/${file}_${type}.csv"
fileOut="GNUPLOT-DW/${file}_${type}.gp"
fileImg="IMG-DW/${file}_${type}.png"

code="
reset
set datafile separator ';'
set xlabel 'Weight'
set ylabel '$title'
set key off
set grid y
set autoscale y
set autoscale x
set style data yerrorlines
set terminal pngcairo size 640,480 enhanced font 'Verdana,13'
set output '${fileImg}'
plot '${fileIn}' every ::1 notitle
"
echo "$code" > $fileOut

gnuplot $fileOut

done
done