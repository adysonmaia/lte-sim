<?php
$runs = 30;
$execTime = 5.0; // seconds
$h2hTypes = array (
		"CBR",
		"VIDEO",
		"VOIP" 
);
$m2mTypes = array (
		"M2M_TIME",
		"M2M_EVENT" 
);
$weights = array (
		"0",
		"0.1",
		"0.2",
		"0.3",
		"0.4",
		"0.5",
		"0.6",
		"0.7",
		"0.8",
		"0.9",
		"0.95",
		"1" 
);
$allTypes = array_merge ( $h2hTypes, $m2mTypes );
function getStudentTDistribution($degree) {
	$table = array (
			0.0,
			12.70620474,
			4.30265273,
			3.18244631,
			2.77644511,
			2.57058184,
			2.44691185,
			2.36462425,
			2.30600414,
			2.26215716,
			2.22813885,
			2.20098516,
			2.17881283,
			2.16036866,
			2.14478669,
			2.13144955,
			2.11990530,
			2.10981558,
			2.10092204,
			2.09302406,
			2.08596345,
			2.07961385,
			2.07387307,
			2.06865761,
			2.06389857,
			2.05953856,
			2.05552944,
			2.05183052,
			2.04840714,
			2.04522964,
			2.04227246,
			2.03951345,
			2.03693334,
			2.03451530,
			2.03224451,
			2.03010793,
			2.0280940,
			2.02619246,
			2.02439416,
			2.02269092,
			2.02107539 
	);
	return $table [$degree];
}

$data = array ();
for($run = 0; $run < $runs; $run ++) {
	foreach ( $weights as $weight ) {
		$bearers = array ();
		$types = array ();
		
		$fileIn = "TRACE-DW/SCHED_{$weight}_{$run}";
		if (! file_exists ( $fileIn )) {
			continue;
		}
		$handle = fopen ( $fileIn, "r" );
		if (! $handle) {
			continue;
		}
		while ( ($line = fgets ( $handle )) !== false ) {
			$parts = explode ( " ", $line );
			if (count ( $parts ) < 8)
				continue;
			
			$txRx = $parts [0];
			$type = $parts [1];
			
			if (!in_array($type, $allTypes)) {
				continue;
			}
			
			if ($txRx == "POWER") {
				$bearer = ( int ) $parts [3];
				$size = ( double ) pow ( 10, $parts [7] / 10.0 ); // dBm -> mW
			} else {
				$bearer = ( int ) $parts [5];
				$size = ( int ) $parts [7];
			}
			
			if (! isset ( $bearers [$bearer] [$txRx] )) {
				$bearers [$bearer] [$txRx] = array (
						"size" => 0,
						"count" => 0 
				);
			}
			$bearers [$bearer] [$txRx] ["size"] += $size;
			$bearers [$bearer] [$txRx] ["count"] ++;
			
			if (! isset ( $types [$type] )) {
				$types [$type] = array ();
			}
			if (! in_array ( $bearer, $types [$type] )) {
				$types [$type] [] = $bearer;
			}
			
			$groupType = "";
			if (in_array ( $type, $h2hTypes )) {
				$groupType = "H2H";
			} else if (in_array ( $type, $m2mTypes )) {
				$groupType = "M2M";
			}
			
			if (! empty ( $groupType )) {
				if (! isset ( $types [$groupType] )) {
					$types [$groupType] = array ();
				}
				if (! in_array ( $bearer, $types [$groupType] )) {
					$types [$groupType] [] = $bearer;
				}
			}
		}
		
		foreach ( $types as $type => $typeBearers ) {
			$powerTotal = 0;
			$rxTotalSize = 0;
			$packetsTxCount = 0;
			$packetsRxCount = 0;
			$sumQuad = 0;
			$sum = 0;
			
			foreach ( $typeBearers as $bearerKey ) {
				$bearer = $bearers [$bearerKey];
				if (array_key_exists ( "RX", $bearer )) {
					$rxTotalSize += $bearer ["RX"] ["size"];
					$packetsRxCount += $bearer ["RX"] ["count"];
					$rxSize = $bearer ["RX"] ["size"] / 1024.0;
					$sum += $rxSize;
					$sumQuad += $rxSize * $rxSize;
				}
				if (array_key_exists ( "TX", $bearer )) {
					$packetsTxCount += $bearer ["TX"] ["count"];
				}
				if (array_key_exists ( "POWER", $bearer ) && $bearer ["POWER"] ["count"] > 0) {
					$powerTotal += $bearer ["POWER"] ["size"];
				}
			}
			
			$geralThroughput = 8.0 * ($rxTotalSize / ($execTime * 1024.0 * 1024.0));
			$fairnessIndex = 0;
			$packetLossRate = 0;
			if (count ( $typeBearers ) > 0 && $sumQuad > 0.0) {
				$fairnessIndex = ($sum * $sum) / ( double ) (count ( $typeBearers ) * $sumQuad);
			}
			if ($packetsTxCount > 0) {
				$packetLossRate = 100.0 * ($packetsTxCount - $packetsRxCount) / ( double ) $packetsTxCount;
				if ($packetLossRate < 0.0)
					$packetLossRate = 0.0;
			}
			if ($powerTotal > 0) {
				$powerTotal = $rxTotalSize / ( double ) $powerTotal;
			}
			
			$data ["TPUT"] [$type] [$weight] [$run] = $geralThroughput;
			$data ["FAIR"] [$type] [$weight] [$run] = $fairnessIndex;
			$data ["PLR"] [$type] [$weight] [$run] = $packetLossRate;
			$data ["PW"] [$type] [$weight] [$run] = $powerTotal;
		}
		
		unset ( $bearers );
		unset ( $types );
	}
}

foreach ( $data as $metric => $types ) {
	foreach ( $types as $type => $weights ) {
		$fileOut = "CSV-DW/{$metric}_{$type}.csv";
		$csv = "";
		$line = array (
				"Deadline Weight",
				$metric,
				"",
				"" 
		);
		$csv .= implode ( ";", $line ) . "\n";
		foreach ( $weights as $weight => $runs ) {
			$line = array (
					$weight 
			);
			$avg = 0;
			foreach ( $runs as $run ) {
				$avg += $run;
			}
			
			$count = count ( $runs );
			if ($count > 0) {
				$avg /= ( double ) $count;
			}
			$line [] = $avg;
			
			$stdDvt = 0.0;
			foreach ( $runs as $run ) {
				$stdDvt += ($run - $avg) * ($run - $avg);
			}
			$error = 0.0;
			if ($count > 1) {
				$stdDvt = sqrt ( $stdDvt / ( double ) ($count - 1.0) );
				$error = getStudentTDistribution ( $count - 1 ) * ($stdDvt / sqrt ( $count ));
			} else {
				$stdDvt = 0.0;
			}
			$line [] = $avg - $error;
			$line [] = $avg + $error;
			$csv .= implode ( ";", $line ) . "\n";
		}
		file_put_contents ( $fileOut, $csv );
	}
}
