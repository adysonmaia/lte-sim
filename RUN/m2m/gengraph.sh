#!/bin/bash
#set -x

rm -f GNUPLOT/*
rm -f IMG/*

nbSchedulers=10
#nbSchedulers=5
plotUsing=""
i=1
until [ $i -gt $nbSchedulers ]; do

col1=$((3*$i - 1))
col2=$(($col1 + 1))
col3=$(($col2 + 1))

if [ -z "$plotUsing"  ]; then
plotUsing="using 1:${col1}:${col2}:${col3}"
else
plotUsing="$plotUsing, '' using 1:${col1}:${col2}:${col3}"
fi

i=$(($i + 1))
done

for file in "PLR" "TPUT" "FAIR" "PW"; do
title=""
if [ $file == "PLR" ]; then
title="Packets not meeting delay constraint [%]"
elif [ $file == "TPUT" ]; then
title="Throughput [mbps]"
elif [ $file == "FAIR" ]; then
title="Fairness Index"
elif [ $file == "PW" ]; then
title="Power Efficiency [byte/dBm]"
fi

for type in VOIP VIDEO CBR M2M_TIME M2M_EVENT M2M H2H; do
fileIn="CSV/${file}_${type}.csv"
fileOut="GNUPLOT/${file}_${type}.gp"
fileImg="IMG/${file}_${type}.png"

code="
reset
set datafile separator ';'
set xlabel 'Number of M2M devices'
set ylabel '$title'
set key font 'Verdana,10'
set key below box
set key samplen 2
set key autotitle columnhead
set grid y
set autoscale y
set autoscale x
set style data yerrorlines
set style line 1 pointtype 4
set style line 2 pointtype 4
set style line 3 pointtype 6
set style line 4 pointtype 8
set style line 5 pointtype 10
set style line 6 pointtype 12
set style line 7 pointtype 2
set style line 8 pointtype 2
set style line 9 pointtype 7
set style line 10 pointtype 7
set style increment user
set terminal pngcairo size 640,510 enhanced font 'Verdana,13'
set output '${fileImg}'
plot '${fileIn}' ${plotUsing}
"
echo "$code" > $fileOut

gnuplot $fileOut

done
done