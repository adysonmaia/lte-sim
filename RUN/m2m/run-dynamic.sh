set -x
set -e

_SIM=../../Release/lte-sim
#_SIM=../../Debug/lte-sim
_COUNT=0
_NB_SEEDS=30
_TRACE_DIR=TRACE-DYNAMIC

if [ ! -d ${_TRACE_DIR} ]; then
    mkdir -p $_TRACE_DIR
fi

until [ $_COUNT -gt $_NB_SEEDS ]; do
for low in 0.1 0.2 0.3 0.4 0.5; do
for high in 0.5 0.6 0.7 0.8 0.9; do
trace=${_TRACE_DIR}/SCHED_${low}_${high}_${_COUNT}

if [ ! -f $trace ]; then
    time $_SIM M2M-DYNAMIC $low $high $_COUNT > $trace
fi

done
done
_COUNT=$(($_COUNT + 1))
done
