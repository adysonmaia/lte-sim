<?php
$runs = 30;
$execTime = 5.0; // seconds
$h2hTypes = array (
		"CBR",
		"VIDEO",
		"VOIP" 
);
$m2mTypes = array (
		"M2M_TIME",
		"M2M_EVENT" 
);

$lowLimits = array (
		"0.1",
		"0.2",
		"0.3",
		"0.4",
		"0.5" 
);

$highLimits = array (
		"0.5",
		"0.6",
		"0.7",
		"0.8",
		"0.9" 
);

$data = array ();
for($run = 0; $run < $runs; $run ++) {
	foreach ( $lowLimits as $low ) {
		foreach ( $highLimits as $high ) {
			$bearers = array ();
			$types = array ();
			
			$fileIn = "TRACE-DYNAMIC/SCHED_{$low}_{$high}_{$run}";
			if (! file_exists ( $fileIn )) {
				continue;
			}
			$handle = fopen ( $fileIn, "r" );
			if (! $handle) {
				continue;
			}
			while ( ($line = fgets ( $handle )) !== false ) {
				$parts = explode ( " ", $line );
				if (count ( $parts ) < 8)
					continue;
				
				$txRx = $parts [0];
				$type = $parts [1];
				if ($txRx == "POWER") {
					$bearer = ( int ) $parts [3];
					$size = ( double ) pow ( 10, $parts [7] / 10.0 ); // dBm -> mW
				} else {
					$bearer = ( int ) $parts [5];
					$size = ( int ) $parts [7];
				}
				
				if (! isset ( $bearers [$bearer] [$txRx] )) {
					$bearers [$bearer] [$txRx] = array (
							"size" => 0,
							"count" => 0 
					);
				}
				$bearers [$bearer] [$txRx] ["size"] += $size;
				$bearers [$bearer] [$txRx] ["count"] ++;
				
				if (! isset ( $types [$type] )) {
					$types [$type] = array ();
				}
				if (! in_array ( $bearer, $types [$type] )) {
					$types [$type] [] = $bearer;
				}
				
				$groupType = "";
				if (in_array ( $type, $h2hTypes )) {
					$groupType = "H2H";
				} else if (in_array ( $type, $m2mTypes )) {
					$groupType = "M2M";
				}
				
				if (! empty ( $groupType )) {
					if (! isset ( $types [$groupType] )) {
						$types [$groupType] = array ();
					}
					if (! in_array ( $bearer, $types [$groupType] )) {
						$types [$groupType] [] = $bearer;
					}
				}
			}
			
			foreach ( $types as $type => $typeBearers ) {
				$powerTotal = 0;
				$rxTotalSize = 0;
				$packetsTxCount = 0;
				$packetsRxCount = 0;
				$sumQuad = 0;
				$sum = 0;
				
				foreach ( $typeBearers as $bearerKey ) {
					$bearer = $bearers [$bearerKey];
					if (array_key_exists ( "RX", $bearer )) {
						$rxTotalSize += $bearer ["RX"] ["size"];
						$packetsRxCount += $bearer ["RX"] ["count"];
						$rxSize = $bearer ["RX"] ["size"] / 1024.0;
						$sum += $rxSize;
						$sumQuad += $rxSize * $rxSize;
					}
					if (array_key_exists ( "TX", $bearer )) {
						$packetsTxCount += $bearer ["TX"] ["count"];
					}
					if (array_key_exists ( "POWER", $bearer ) && $bearer ["POWER"] ["count"] > 0) {
						$powerTotal += $bearer ["POWER"] ["size"];
					}
				}
				
				$geralThroughput = 8.0 * ($rxTotalSize / ($execTime * 1024.0 * 1024.0));
				$fairnessIndex = 0;
				$packetLossRate = 0;
				if (count ( $typeBearers ) > 0 && $sumQuad > 0.0) {
					$fairnessIndex = ($sum * $sum) / ( double ) (count ( $typeBearers ) * $sumQuad);
				}
				if ($packetsTxCount > 0) {
					$packetLossRate = 100.0 * ($packetsTxCount - $packetsRxCount) / ( double ) $packetsTxCount;
					if ($packetLossRate < 0.0)
						$packetLossRate = 0.0;
				}
				if ($powerTotal > 0) {
					$powerTotal = $rxTotalSize / ( double ) $powerTotal;
				}
				
				$data ["TPUT"] [$type] [$low] [$high] [$run] = $geralThroughput;
				$data ["FAIR"] [$type] [$low] [$high] [$run] = $fairnessIndex;
				$data ["PLR"] [$type] [$low] [$high] [$run] = $packetLossRate;
				$data ["PW"] [$type] [$low] [$high] [$run] = $powerTotal;
			}
			
			unset ( $bearers );
			unset ( $types );
		}
	}
}

foreach ( $data as $metric => $types ) {
	foreach ( $types as $type => $lowValues ) {
		$fileOut = "CSV-DYNAMIC/{$metric}_{$type}.csv";
		$csv = "";
		$line = array (
				"Low",
				"High",
				"$metric" 
		);
		//$line = array ();
		$csv .= implode ( ";", $line ) . "\n";
		foreach ( $lowValues as $low => $highValues ) {
			foreach ( $highValues as $high => $runs ) {
				$line = array (
						$low,
						$high 
				);
				$avg = 0;
				foreach ( $runs as $run ) {
					$avg += $run;
				}
				
				$count = count ( $runs );
				if ($count > 0) {
					$avg /= ( double ) $count;
				}
				$line [] = $avg;
				$csv .= implode ( ";", $line ) . "\n";
			}
			$csv .= "\n";
			file_put_contents ( $fileOut, $csv );
		}
	}
}
