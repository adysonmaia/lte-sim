<?php
$runs = 30;
$ues = array (
		0,
		200,
		400,
		600,
		800,
		1000 
);
$scheds = array (
		6,
		8,
		10,
		12,
		13,
		15 
);
function getCDF($samples) {
	$max = ( int ) max ( $samples );
	$min = ( int ) min ( $samples );
	$size = count ( $samples );
	$cdf = array ();
	for($i = $min; $i <= $max; $i ++) {
		$cdf [$i] = 0;
		foreach ( $samples as $value ) {
			if ($value <= $i) {
				$cdf [$i] ++;
			}
		}
		$cdf [$i] = $cdf [$i] / ( double ) $size;
	}
	return $cdf;
}
$data = array ();
for($run = 0; $run < $runs; $run ++) {
	foreach ( $ues as $ue ) {
		foreach ( $scheds as $sched ) {
			$bearers = array ();
			$types = array ();
			$data [$run] [$ue] [$sched] = array ();
			
			$fileIn = "TRACE/SCHED_{$sched}_UE_{$ue}_{$run}";
			if (! file_exists ( $fileIn )) {
				continue;
			}
			$handle = fopen ( $fileIn, "r" );
			if (! $handle) {
				continue;
			}
			while ( ($line = fgets ( $handle )) !== false ) {
				$parts = explode ( " ", $line );
				if (count ( $parts ) < 8)
					continue;
				
				$txRx = $parts [0];
				$type = $parts [1];
				if ($txRx == "POWER") {
					$bearer = ( int ) $parts [3];
					$value = ( double ) $parts [7];
					
					if (! isset ( $data [$run] [$ue] [$sched] [$type] )) {
						$data [$run] [$ue] [$sched] [$type] = array ();
					}
					$data [$run] [$ue] [$sched] [$type] [] = $value;
				}
			}
			
			foreach ($data [$run] [$ue] [$sched] as  $type => $samples) {
				
			}
		}
	}
}