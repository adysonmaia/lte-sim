/*
 * up-m2m-packet-scheduler.cpp
 *
 *  Created on: 22/07/2014
 *      Author: adyson
 */

#include "up-m2m-packet-scheduler.h"
#include "../mac-entity.h"
#include "../AMCModule.h"
#include "../../../flows/radio-bearer.h"
#include "../../../flows/QoS/QoSParameters.h"
#include "../../../device/NetworkNode.h"
#include "../../../phy/lte-phy.h"
#include "../../../utility/eesm-effective-sinr.h"
#include "../../../core/spectrum/bandwidth-manager.h"
#include "../../../componentManagers/FrameManager.h"
#include "../../../utility/ConfigParameters.h"
#include "../../../load-parameters.h"
#include <cmath>
#include <cfloat>
#include <utility>
#include <vector>
#include <algorithm>

static bool compareMetrics(const std::pair<UplinkPacketScheduler::UserToSchedule*, double> &i,
		const std::pair<UplinkPacketScheduler::UserToSchedule*, double> &j) {
	return (i.second > j.second);
}

UP_M2M_PacketScheduler::UP_M2M_PacketScheduler() :
		m_version(1), m_minRBsH2H(4), m_minRBsM2M(4), m_minPercentRBsM2M(0.4), m_requestDeniedSpread(0.1), m_deadlineMetricWeight(
				0.9), m_congestionTimeWindow(2.0), m_congestion(0.0), m_minPercentRBsM2MLow(0.32), m_minPercentRBsM2MNormal(
				0.4), m_minPercentRBsM2MHigh(0.48), m_congestionLow(0.2), m_congestionHigh(0.6) {

	ConfigParameters *config = ConfigParameters::Init();
	m_minRBsH2H = config->GetIntValue(CONF_PARAM_RB_H2H, m_minRBsH2H);
	m_minRBsM2M = config->GetIntValue(CONF_PARAM_RB_M2M, m_minRBsM2M);
	m_minPercentRBsM2M = config->GetDoubleValue(CONF_PARAM_PERCENT_RB_M2M, m_minPercentRBsM2M);
	m_requestDeniedSpread = config->GetDoubleValue(CONF_PARAM_REQUEST_SPREAD, m_requestDeniedSpread);
	m_deadlineMetricWeight = config->GetDoubleValue(CONF_PARAM_DEADLINE_WEIGHT, m_deadlineMetricWeight);
	m_congestionTimeWindow = config->GetDoubleValue(CONF_PARAM_CONGESTION_TW, m_congestionTimeWindow);
	m_minPercentRBsM2MLow = config->GetDoubleValue(CONF_PARAM_PERCENT_RB_LOW, m_minPercentRBsM2MLow);
	m_minPercentRBsM2MNormal = config->GetDoubleValue(CONF_PARAM_PERCENT_RB_NORMAL, m_minPercentRBsM2MNormal);
	m_minPercentRBsM2MHigh = config->GetDoubleValue(CONF_PARAM_PERCENT_RB_HIGH, m_minPercentRBsM2MHigh);
	m_congestionLow = config->GetDoubleValue(CONF_PARAM_CONGESTION_LOW, m_congestionLow);
	m_congestionHigh = config->GetDoubleValue(CONF_PARAM_CONGESTION_HIGH, m_congestionHigh);
}

UP_M2M_PacketScheduler::~UP_M2M_PacketScheduler() {
}

void UP_M2M_PacketScheduler::RBsAllocation() {
	int nbRBs = GetMacEntity()->GetDevice()->GetPhy()->GetBandwidthManager()->GetUlSubChannels().size();
	int demandRBsForH2H = 0;

	UpdateGrant();

	UsersToSchedule *users = GetUsersToSchedule();
	UsersToSchedule::iterator itUser;
	UsersToSchedule *m2mUsers = new UsersToSchedule[users->size()];
	UsersToSchedule *h2hUsers = new UsersToSchedule[users->size()];

	for (itUser = users->begin(); itUser != users->end(); itUser++) {
		UserToSchedule* user = *itUser;
		if (!HasGrant(user))
			continue;
		if (IsM2M(user)) {
			m2mUsers->push_back(user);
		} else {
			UP_PS_Statistic *stats = this->GetStatistic(user);
			if (stats->averageReceivedBytes > 0.0) {
				double demand = (user->m_dataToTransmit * stats->averageRBsAllocated) / stats->averageReceivedBytes;
				demandRBsForH2H += std::max(this->m_minRBsH2H, (int) ceil(demand));
			} else {
				demandRBsForH2H += this->m_minRBsH2H;
			}
			h2hUsers->push_back(user);
		}
	}
#ifdef M2M_SCHEDULER_DEBUG
	std::cout << "\n" << FrameManager::Init()->GetTTICounter() << " H2H " << h2hUsers->size() << " M2M "
			<< m2mUsers->size() << std::endl;
#endif

	demandRBsForH2H = std::min(demandRBsForH2H, nbRBs);
	int demandRBsForM2M = (int) std::max(floor(this->m_minPercentRBsM2M * nbRBs / (double) m_minRBsM2M),
			floor((nbRBs - demandRBsForH2H) / (double) m_minRBsM2M));
	int nbRBsForM2M = m_minRBsM2M * std::min(demandRBsForM2M, (int) m2mUsers->size());
	int lastRB = DoM2MSchedule(m2mUsers, 0, nbRBsForM2M);

	int rbStartForH2H = lastRB + 1;
	int nbRBsForH2H = nbRBs - rbStartForH2H;
	if (nbRBsForH2H > 0)
		DoH2HSchedule(h2hUsers, rbStartForH2H, nbRBsForH2H);

#ifdef M2M_SCHEDULER_DEBUG
	std::cout << "--" << std::endl;
#endif

	//UpdateStatistic();
	if (this->m_version == 2)
		this->UpdateCongestion(m2mUsers);
	delete[] m2mUsers;
	delete[] h2hUsers;
}

int UP_M2M_PacketScheduler::DoM2MSchedule(UsersToSchedule *usersToSchedule, int rbStart, int nbRBs) {
	double maxThroughput = 0.0;
	double maxDeadline = 0.0;
	std::map<UserToSchedule*, double> deadlines;
	UsersToSchedule::iterator itUser;
	for (itUser = usersToSchedule->begin(); itUser != usersToSchedule->end(); itUser++) {
		UserToSchedule* user = *itUser;
		UP_PS_Statistic *stats = this->GetStatistic(user);
		if (stats->averageTransmissionRate > maxThroughput)
			maxThroughput = stats->averageTransmissionRate;
		double deadline = this->GetDeadline(user);
		if (deadline < 0.0)
			deadline = 0.0;
		if (deadline > maxDeadline)
			maxDeadline = deadline;
		deadlines.insert(std::make_pair(user, deadline));
	}

	std::vector<std::pair<UserToSchedule*, double> > metrics;
	for (itUser = usersToSchedule->begin(); itUser != usersToSchedule->end(); itUser++) {
		UserToSchedule* user = *itUser;
		UP_PS_Statistic *stats = this->GetStatistic(user);
		double deadline = deadlines.at(user);
		double throughput = stats->averageTransmissionRate;
		double metricDeadline = 1.0;
		double metricThrougput = 1.0;
		if (maxThroughput > 0.0)
			metricThrougput = 1.0 - throughput / maxThroughput;
		if (maxDeadline > 0.0)
			maxDeadline = 1.0 - deadline / maxDeadline;
		double metric = (1.0 - this->m_deadlineMetricWeight) * metricThrougput
				+ this->m_deadlineMetricWeight * metricDeadline;
		metrics.push_back(std::make_pair(user, metric));
	}

	int lastRB = -1;
	int nextRB = 0;
	int nbRBsAvailable = nbRBs;
	std::sort(metrics.begin(), metrics.end(), compareMetrics);
	std::vector<std::pair<UserToSchedule*, double> >::iterator itMetric;
	for (itMetric = metrics.begin(); itMetric != metrics.end() && nbRBsAvailable > 0; itMetric++) {
		UserToSchedule* user = itMetric->first;
		if (nbRBsAvailable > 0) {
			int firstRB = nextRB;
			int size = this->m_minRBsM2M;
			double specEffic = GetSpectralEfficiency(user, firstRB + rbStart, size);
			int mcs = GetMacEntity()->GetAmcModule()->GetMCSFromCQI(
					GetMacEntity()->GetAmcModule()->GetCQIFromSinr(specEffic));
			int tbs = (GetMacEntity()->GetAmcModule()->GetTBSizeFromMCS(mcs, size)) / 8;
			user->m_selectedMCS = mcs;
			user->m_transmittedData = tbs;
			for (int i = 0; i < size; i++) {
				user->m_listOfAllocatedRBs.push_back(i + firstRB + rbStart);
				nbRBsAvailable--;
			}
			nextRB = firstRB + size;
			lastRB = nextRB - 1;

#ifdef M2M_SCHEDULER_DEBUG
			std::cout << "ID " << user->m_userToSchedule->GetIDNetworkNode() << " data " << user->m_dataToTransmit
					<< " tbs " << tbs << " rbStart " << user->m_listOfAllocatedRBs[0] << " rbSize "
					<< user->m_listOfAllocatedRBs.size() << " metric " << itMetric->second << std::endl;
#endif

		} else {
//			double delay = this->GetUpBearer(user)->GetQoSParameters()->GetMaxDelay() * 1000; // s -> ms
			double delay = deadlines.at(user) * 1000;
			int maxDelayTime = (int) floor(std::max(2.0, delay * this->m_requestDeniedSpread));
			int waitTime = 1 + rand() % maxDelayTime;
			this->SetGrant(user, waitTime);
		}
	}

	return (lastRB + rbStart);
}

double UP_M2M_PacketScheduler::GetDeadline(UserToSchedule* user) {
	RadioBearer *bearer = this->GetUpBearer(user);
	double delay = bearer->GetQoSParameters()->GetMaxDelay();
	double deadline = delay + user->m_lastRequestTime - Simulator::Init()->Now();
	return deadline;
}

void UP_M2M_PacketScheduler::UpdateCongestion(UsersToSchedule *users) {
	int countReceivedRb = 0;
	UsersToSchedule::iterator itUser;
	for (itUser = users->begin(); itUser != users->end(); itUser++) {
		UserToSchedule* user = *itUser;
		if (user->m_listOfAllocatedRBs.size() > 0)
			countReceivedRb++;
	}

	double congestionLevel = 0;
	if (users->size() > 0)
		congestionLevel = 1.0 - countReceivedRb / (double) users->size();

	if (this->m_congestionTimeWindow > 0) {
		this->m_congestion = (1.0 / this->m_congestionTimeWindow) * congestionLevel
				+ (1.0 - (1.0 / this->m_congestionTimeWindow)) * m_congestion;
	} else {
		this->m_congestion = congestionLevel;
	}
	if (this->m_congestion > this->m_congestionHigh) {
		this->m_minPercentRBsM2M = this->m_minPercentRBsM2MHigh;
	} else if (this->m_congestion < this->m_congestionLow) {
		this->m_minPercentRBsM2M = this->m_minPercentRBsM2MLow;
	} else {
		this->m_minPercentRBsM2M = this->m_minPercentRBsM2MNormal;
	}
}
