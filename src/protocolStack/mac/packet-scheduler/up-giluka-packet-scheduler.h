/*
 * up-giluka-packet-scheduler.h
 *
 *  Created on: 13/05/2014
 *      Author: adyson
 */

#ifndef UP_GILUKA_PACKET_SCHEDULER_H_
#define UP_GILUKA_PACKET_SCHEDULER_H_

#include "up-pf-packet-scheduler.h"
#include <vector>
#include <utility>

class UP_Giluka_PacketScheduler: public UP_PF_PacketScheduler {
public:
	UP_Giluka_PacketScheduler();
	virtual ~UP_Giluka_PacketScheduler();

	virtual void RBsAllocation();
protected:
	int DoClassSchedule(std::vector<std::pair<double, UserToSchedule*> > *users, int rbStart, int nbRBs);
};

#endif /* UP_GILUKA_PACKET_SCHEDULER_H_ */
