/*
 * up-lien-packet-scheduler.h
 *
 *  Created on: 09/06/2014
 *      Author: adyson
 */

#ifndef UP_LIEN_PACKET_SCHEDULER_H_
#define UP_LIEN_PACKET_SCHEDULER_H_

#include "up-abdalla-packet-scheduler.h"
#include "up-pf-packet-scheduler.h"
#include <map>

class UP_Lien_PacketScheduler: public UP_Abdalla_PacketScheduler {
public:
	UP_Lien_PacketScheduler();
	virtual ~UP_Lien_PacketScheduler();
private:
	virtual int DoM2MSchedule(UsersToSchedule *users, int rbStart, int nbRBs);
	virtual int DoH2HSchedule(UsersToSchedule *users, int rbStart, int nbRBs);
	virtual bool HasGrant(UserToSchedule *user);
	virtual void SetGrant(UserToSchedule *user, int time);
	int GetAgti(double delay);
	int GetAgti(UserToSchedule *user);

	int m_rbsPerM2M;
	std::map<int, int> m_agtiFirstTTI;
};

#endif /* UP_LIEN_PACKET_SCHEDULER_H_ */
