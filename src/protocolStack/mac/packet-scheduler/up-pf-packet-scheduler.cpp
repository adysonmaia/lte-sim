/*
 * up-pf-packet-scheduler.cpp
 *
 *  Created on: 06/05/2014
 *      Author: adyson
 */

#include "up-pf-packet-scheduler.h"
#include "../mac-entity.h"
#include "../AMCModule.h"
#include "../../../flows/radio-bearer.h"
#include "../../../flows/QoS/QoSParameters.h"
#include "../../../device/NetworkNode.h"
#include "../../../phy/lte-phy.h"
#include "../../../utility/eesm-effective-sinr.h"
#include "../../../core/spectrum/bandwidth-manager.h"
#include "../../../componentManagers/FrameManager.h"
#include "../../../load-parameters.h"
#include <cmath>
#include <utility>
#include <vector>
#include <algorithm>

static bool compareMetrics(const std::pair<double, std::pair<UplinkPacketScheduler::UserToSchedule*, int> > &i,
		const std::pair<double, std::pair<UplinkPacketScheduler::UserToSchedule*, int> > &j) {

	return (i.first > j.first);
}

// ---------------------------------------------------------------

ChunkMap::ChunkMap(int nbRBs) {
	this->nbRBs = nbRBs;
	this->nbChunks = (int) (0.5 * nbRBs * (nbRBs + 1));
	this->chunks = new struct Chunk_s[nbChunks];

	int chunkSize = 1;
	int chunkStart = 0;
	for (int id = 0; id < nbChunks; id++) {
		struct Chunk_s chunk;
		chunk.id = id;
		chunk.available = true;
		chunk.size = chunkSize;
		chunk.start = chunkStart;
		chunks[id] = chunk;

		if (chunkStart + chunkSize >= nbRBs) {
			chunkSize++;
			chunkStart = 0;
		} else {
			chunkStart++;
		}
	}
}

ChunkMap::~ChunkMap() {
	delete[] chunks;
}

void ChunkMap::AllocateChunk(int id) {
	chunks[id].available = false;
	int chunkStart = chunks[id].start;
	int chunkSize = chunks[id].size;
	int chunkEnd = chunkStart + chunkSize - 1;
	for (int id = 0; id < nbChunks; id++) {
		struct Chunk_s *chunk = &chunks[id];
		if (!chunk->available)
			continue;
		int start = chunk->start;
		int end = chunk->start + chunk->size - 1;
		if ((start >= chunkStart && start <= chunkEnd) || (end <= chunkEnd && chunkStart <= end)
				|| (start <= chunkStart && chunkEnd <= end)) {
			chunk->available = false;
		}
	}
}

struct ChunkMap::Chunk_s ChunkMap::GetChunk(int start, int size) {
	int index = ((size - 1) * (2 * nbRBs + 2 - size)) / 2 + start;
	return chunks[index];
//	for (int i = 0; i < nbChunks; i++) {
//		struct Chunk_s chunk = chunks[i];
//		if (chunk.start == start && chunk.size == size)
//			return chunk;
//	}
//	return chunks[0];
}

// ------------------------------------------------------

RBsAllocationMap::RBsAllocationMap(const int size) {
	struct RBsMapValue_s mapValue;
	mapValue.allocated = false;
	mapValue.ueID = 0;
	m_map.resize(size, mapValue);
}

RBsAllocationMap::~RBsAllocationMap() {
	m_map.clear();
}

bool RBsAllocationMap::IsAvailable(const int indexStart, const int size) const {
	bool free = true;
	for (int j = indexStart; j < indexStart + size; j++) {
		if (m_map.at(j).allocated) {
			free = false;
			break;
		}
	}
	return free;
}

void RBsAllocationMap::Allocate(const int rnti, const int indexStart, const int size) {
	struct RBsMapValue_s mapValue = { true, rnti };
	for (int j = indexStart; j < indexStart + size; j++) {
		m_map.at(j) = mapValue;
	}
}

std::vector<int> RBsAllocationMap::GetIndexes(const int rnti) const {
	std::vector<int> result;
	for (int i = 0; i < m_map.size(); i++) {
		struct RBsMapValue_s mapValue = m_map.at(i);
		if (mapValue.allocated && mapValue.ueID == rnti) {
			result.push_back(i);
		}
	}
	return result;
}

bool RBsAllocationMap::HasResources(const int ueID) const {
	std::vector<RBsMapValue_s>::const_iterator it = m_map.begin();
	bool response = false;
	while (it != m_map.end()) {
		if ((*it).allocated && (*it).ueID == ueID) {
			response = true;
			break;
		}
		it++;
	}
	return response;
}

int RBsAllocationMap::GetUeID(const int index) const {
	struct RBsMapValue_s mapValue = m_map.at(index);
	return mapValue.ueID;
}

int RBsAllocationMap::GetSize() const {
	return m_map.size();
}

int RBsAllocationMap::GetAvailableRBsSize() const {
	std::vector<RBsMapValue_s>::const_iterator it = m_map.begin();
	int response = 0;
	while (it != m_map.end()) {
		if (!(*it).allocated) {
			response++;
		}
		it++;
	}
	return response;
}

int RBsAllocationMap::GetFirstAvailableRB() const {
	for (int i = 0; i < m_map.size(); i++) {
		struct RBsMapValue_s mapValue = m_map.at(i);
		if (!mapValue.allocated) {
			return i;
		}
	}
	return m_map.size();
}

void RBsAllocationMap::Clear() {
	std::vector<RBsMapValue_s>::iterator it = m_map.begin();
	while (it != m_map.end()) {
		(*it).allocated = false;
		(*it).ueID = 0;
		it++;
	}
}

// ------------------------------------------------------

UP_PF_PacketScheduler::UP_PF_PacketScheduler() {
	SetMacEntity(0);
	CreateUsersToSchedule();
}

UP_PF_PacketScheduler::~UP_PF_PacketScheduler() {
	Destroy();
}

RadioBearer* UP_PF_PacketScheduler::GetUpBearer(UserToSchedule *user) {
	RrcEntity *rrc = user->m_userToSchedule->GetProtocolStack()->GetRrcEntity();
	RrcEntity::RadioBearersContainer* bearers = rrc->GetRadioBearerContainer();
	for (std::vector<RadioBearer*>::iterator it = bearers->begin(); it != bearers->end(); it++) {
		RadioBearer *bearer = (*it);
//		if (user->m_userToSchedule == bearer->GetSource()) {
		if (user->m_userToSchedule->GetIDNetworkNode() == bearer->GetSource()->GetIDNetworkNode()) {
			return bearer;
		}
	}
	return NULL;
}

bool UP_PF_PacketScheduler::IsM2M(UserToSchedule *user) {
	RadioBearer *bearer = GetUpBearer(user);
	return (bearer->GetQoSParameters()->GetQCI() > 9);
}

UP_PS_Statistic* UP_PF_PacketScheduler::GetStatistic(UserToSchedule* user) {
	int id = user->m_userToSchedule->GetIDNetworkNode();
	std::map<int, UP_PS_Statistic>::iterator it = m_statistic.find(id);
	if (it != m_statistic.end()) {
		return &(it->second);
	} else {
		UP_PS_Statistic stats;
		stats.averageTransmissionRate = 0.0;
		stats.averageRBsAllocated = 0.0;
		stats.averageReceivedBytes = 0.0;
		stats.lastTransmittedBytes = 0;
		stats.lastReceivedBytes = 0;
		stats.lastRBsAllocated = 0;
		stats.totalTransmittedBytes = 0;
		m_statistic.insert(std::pair<int, UP_PS_Statistic>(id, stats));
		return &m_statistic.at(id);
	}
}

void UP_PF_PacketScheduler::UpdateStatistic() {
	std::map<int, UP_PS_Statistic>::iterator itStats;
	for (itStats = m_statistic.begin(); itStats != m_statistic.end(); itStats++) {
		itStats->second.lastTransmittedBytes = 0;
		itStats->second.lastReceivedBytes = 0;
		itStats->second.lastRBsAllocated = 0;
	}

	UsersToSchedule *ptUsers = GetUsersToSchedule();
	UsersToSchedule::iterator itUsers;
	for (itUsers = ptUsers->begin(); itUsers != ptUsers->end(); itUsers++) {
		UserToSchedule *user = *itUsers;
		itStats = m_statistic.find(user->m_userToSchedule->GetIDNetworkNode());
		if (itStats != m_statistic.end()) {
			int data = (*itUsers)->m_transmittedData;
			itStats->second.totalTransmittedBytes += data;
			itStats->second.lastTransmittedBytes = data;
			itStats->second.lastReceivedBytes = user->m_dataToTransmit;
			itStats->second.lastRBsAllocated = user->m_listOfAllocatedRBs.size();
		}
	}

	/*
	 * Update Transmission Data Rate with a Moving Average
	 * R'(t+1) = ( (1 - beta) * R'(t)) + (beta * r(t))
	 */
	double beta = 0.2;
	for (itStats = m_statistic.begin(); itStats != m_statistic.end(); itStats++) {
		itStats->second.averageTransmissionRate = beta * 1000.0 * itStats->second.lastTransmittedBytes
				+ (1 - beta) * itStats->second.averageTransmissionRate;
		itStats->second.averageRBsAllocated = beta * itStats->second.lastRBsAllocated
				+ (1 - beta) * itStats->second.averageRBsAllocated;
		itStats->second.averageReceivedBytes = beta * itStats->second.lastReceivedBytes
				+ (1 - beta) * itStats->second.averageReceivedBytes;
	}
}

bool UP_PF_PacketScheduler::HasGrant(UserToSchedule *user) {
	std::map<int, int>::iterator itGrant = m_grantTimer.find(user->m_userToSchedule->GetIDNetworkNode());
	if (itGrant != m_grantTimer.end()) {
		return (itGrant->second <= 0);
	} else {
		return true;
	}
}

void UP_PF_PacketScheduler::UpdateGrant() {
	std::map<int, int>::iterator itGrant;
	for (itGrant = m_grantTimer.begin(); itGrant != m_grantTimer.end(); itGrant++) {
		if (itGrant->second > 0)
			itGrant->second--;
	}
}

void UP_PF_PacketScheduler::SetGrant(UserToSchedule *user, int time) {
	int key = user->m_userToSchedule->GetIDNetworkNode();
	std::map<int, int>::iterator itGrant = m_grantTimer.find(key);
	if (itGrant != m_grantTimer.end()) {
		itGrant->second = time;
	} else {
		m_grantTimer.insert(std::make_pair(key, time));
	}
}

int UP_PF_PacketScheduler::GetGrant(UserToSchedule *user) {
	std::map<int, int>::iterator itGrant = m_grantTimer.find(user->m_userToSchedule->GetIDNetworkNode());
	if (itGrant != m_grantTimer.end()) {
		return itGrant->second;
	} else {
		return 0;
	}
}

double UP_PF_PacketScheduler::ComputeSchedulingMetric(UserToSchedule* user, double spectralEfficiency,
		int nbSubChannels) {
	int mcs = GetMacEntity()->GetAmcModule()->GetMCSFromCQI(
			GetMacEntity()->GetAmcModule()->GetCQIFromSinr(spectralEfficiency));
	int tbs = (GetMacEntity()->GetAmcModule()->GetTBSizeFromMCS(mcs, nbSubChannels)) / 8;
	double metric;

	UP_PS_Statistic* stat = GetStatistic(user);
//	if (stat->averageTransmissionRate > 0.0)
//		metric = 1000.0 * tbs / stat->averageTransmissionRate;
//	else
//		metric = 1000.0 * tbs;
	if (stat->averageTransmissionRate > 0.0)
		metric = tbs / stat->averageTransmissionRate;
	else
		metric = tbs;
	return metric;
}

double UP_PF_PacketScheduler::ComputeSchedulingMetric(UserToSchedule* user, int rbStart, int nbRBs) {
	int specEffi = GetSpectralEfficiency(user, rbStart, nbRBs);
	int mcs = GetMacEntity()->GetAmcModule()->GetMCSFromCQI(GetMacEntity()->GetAmcModule()->GetCQIFromSinr(specEffi));
	int tbs = (GetMacEntity()->GetAmcModule()->GetTBSizeFromMCS(mcs, nbRBs)) / 8;

	double metric = tbs;
	UP_PS_Statistic* stat = GetStatistic(user);
	if (tbs > user->m_dataToTransmit && nbRBs > 1) {
		int newTbs = (GetMacEntity()->GetAmcModule()->GetTBSizeFromMCS(mcs, nbRBs - 1)) / 8;
		if (newTbs >= user->m_dataToTransmit)
			metric = 0.0;
	} else if (stat->averageTransmissionRate > 0.0) {
		metric = tbs / stat->averageTransmissionRate;
	}
	return metric;
}

double UP_PF_PacketScheduler::GetSpectralEfficiency(UserToSchedule* user, int rbStart, int rbSize) {
	std::vector<double> sinrs;
	for (int rbIndex = rbStart; rbIndex < rbStart + rbSize; rbIndex++) {
		sinrs.push_back(GetMacEntity()->GetAmcModule()->GetSinrFromCQI(user->m_channelContition.at(rbIndex)));
	}
	double effectiveSinr = GetEesmEffectiveSinr(sinrs);
	return effectiveSinr;
}

double UP_PF_PacketScheduler::ComputeSchedulingMetric(RadioBearer *bearer, double spectralEfficiency, int subChannel) {
	return 0.0;
}

double UP_PF_PacketScheduler::ComputeSchedulingMetric(UserToSchedule* user, int subchannel) {
	return 0.0;
}

void UP_PF_PacketScheduler::RBsAllocation() {
	int nbRBs = GetMacEntity()->GetDevice()->GetPhy()->GetBandwidthManager()->GetUlSubChannels().size();
	UsersToSchedule *users = GetUsersToSchedule();

#ifdef M2M_SCHEDULER_DEBUG
	std::cout << "\n" << FrameManager::Init()->GetTTICounter() << " PF Begin users: " << users->size() << std::endl;
#endif

	DoH2HSchedule(users, 0, nbRBs);

#ifdef M2M_SCHEDULER_DEBUG
	std::cout << "--" << std::endl;
#endif
}

int UP_PF_PacketScheduler::DoH2HSchedule(UsersToSchedule *usersToSchedule, int rbStart, int nbRBs) {
	int nbRBsAvailable = nbRBs;
	int maxRBIndex = -1;

	ChunkMap *chunks = new ChunkMap(nbRBs);
	int nbChunks = chunks->GetChunksSize();

	std::vector<UserToSchedule*> users;
	for (int i = 0; i < usersToSchedule->size(); i++) {
		UserToSchedule* user = usersToSchedule->at(i);
		if (user != NULL) {
			GetUpBearer(user);
			users.push_back(user);
		}
	}

	std::vector<UserToSchedule*>::iterator itUser;
	std::vector<std::pair<double, std::pair<UserToSchedule*, int> > > metrics;
	std::vector<std::pair<double, std::pair<UserToSchedule*, int> > >::iterator itMetric;

	for (itUser = users.begin(); itUser != users.end(); itUser++) {
		UserToSchedule* user = *itUser;
		for (int chunkIndex = 0; chunkIndex < nbChunks; chunkIndex++) {
			struct ChunkMap::Chunk_s chunk = chunks->GetChunk(chunkIndex);
			double metric = ComputeSchedulingMetric(user, chunk.start + rbStart, chunk.size);
			if (metric > 0.0) {
				std::pair<UserToSchedule*, int> key = std::make_pair(user, chunkIndex);
				metrics.push_back(std::make_pair(metric, key));
			}
		}
	}
	std::sort(metrics.begin(), metrics.end(), compareMetrics);
	for (itMetric = metrics.begin(); itMetric != metrics.end() && nbRBsAvailable > 0; itMetric++) {
		UserToSchedule* user = itMetric->second.first;
		struct ChunkMap::Chunk_s chunk = chunks->GetChunk(itMetric->second.second);
		if (!chunk.available || user->m_listOfAllocatedRBs.size() > 0)
			continue;

		double spectralEfficiency = GetSpectralEfficiency(user, chunk.start + rbStart, chunk.size);
		int mcs = GetMacEntity()->GetAmcModule()->GetMCSFromCQI(
				GetMacEntity()->GetAmcModule()->GetCQIFromSinr(spectralEfficiency));
		int tbs = (GetMacEntity()->GetAmcModule()->GetTBSizeFromMCS(mcs, chunk.size)) / 8;

		user->m_selectedMCS = mcs;
		user->m_transmittedData = tbs;
		for (int i = 0; i < chunk.size; i++) {
			user->m_listOfAllocatedRBs.push_back(i + chunk.start + rbStart);
		}

		nbRBsAvailable -= chunk.size;
		chunks->AllocateChunk(chunk.id);

		if (chunk.start + chunk.size - 1 > maxRBIndex) {
			maxRBIndex = chunk.start + chunk.size - 1;
		}

#ifdef M2M_SCHEDULER_DEBUG
		UP_PS_Statistic *stats = GetStatistic(user);
		std::cout << "ID " << user->m_userToSchedule->GetIDNetworkNode() << " data " << user->m_dataToTransmit
		<< " tbs " << tbs << " rbStart " << user->m_listOfAllocatedRBs[0] << " rbSize "
		<< user->m_listOfAllocatedRBs.size() << " metric " << itMetric->first << " avg "
		<< stats->averageTransmissionRate << std::endl;
#endif
	}

	UpdateStatistic();
	delete chunks;
	return (maxRBIndex + rbStart);

}

