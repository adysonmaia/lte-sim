/*
 * up-aijaz-packet-scheduler.h
 *
 *  Created on: 12/06/2014
 *      Author: adyson
 */

#ifndef UP_AIJAZ_PACKET_SCHEDULER_H_
#define UP_AIJAZ_PACKET_SCHEDULER_H_

#include "up-pf-packet-scheduler.h"

class UP_Aijaz_PacketScheduler: public UP_PF_PacketScheduler {
public:
	UP_Aijaz_PacketScheduler();
	virtual ~UP_Aijaz_PacketScheduler();
	virtual void RBsAllocation();

	inline void SetVersion(int version) {
		this->m_version = version;
	}
	inline int GetVersion() const {
		return this->m_version;
	}
	inline void SetMaxRBsForM2M(double value) {
		this->m_maxRBsForM2M = value;
	}
	inline double GetMaxRBsForM2M() const {
		return this->m_maxRBsForM2M;
	}
protected:
	int DoScheduleV1(UsersToSchedule *allUsers, UsersToSchedule *m2mUsers, int rbStart, int nbRBs);
	virtual int DoH2HSchedule(UsersToSchedule *users, int rbStart, int nbRBs);
	virtual int DoM2MSchedule(UsersToSchedule *users, int rbStart, int nbRBs);
	int DoBodySchedule(UsersToSchedule *allUsers, UsersToSchedule *m2mUsers, int rbStart, int nbRBs);
	int ComputeTBSize(UserToSchedule *user, int rbStart, int nbRBs);
	double ComputeMeanDelay(UsersToSchedule *users, UserToSchedule *userException);

	int m_version;
	double m_maxRBsForM2M;
};

#endif /* UP_AIJAZ_PACKET_SCHEDULER_H_ */
