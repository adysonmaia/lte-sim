/*
 * up-lioumpas-packetS-scheduler.cpp
 *
 *  Created on: 10/05/2014
 *      Author: adyson
 */

#include "up-lioumpas-packet-scheduler.h"
#include "../mac-entity.h"
#include "../AMCModule.h"
#include "../../../flows/radio-bearer.h"
#include "../../../flows/QoS/QoSParameters.h"
#include "../../../device/NetworkNode.h"
#include "../../../phy/lte-phy.h"
#include "../../../utility/eesm-effective-sinr.h"
#include "../../../core/spectrum/bandwidth-manager.h"
#include "../../../componentManagers/FrameManager.h"
#include "../../../utility/ConfigParameters.h"
#include "../../../load-parameters.h"
#include <cmath>
#include <algorithm>
#include <vector>

static bool compareMetricsV2(const std::pair<UplinkPacketScheduler::UserToSchedule*, double> &i,
		const std::pair<UplinkPacketScheduler::UserToSchedule*, double> &j) {
	return (i.second < j.second);
}

static bool compareMetricsV1(const std::pair<int, std::pair<UplinkPacketScheduler::UserToSchedule*, int> > &i,
		const std::pair<int, std::pair<UplinkPacketScheduler::UserToSchedule*, int> > &j) {
	return (i.first > j.first);
}

UP_Lioumpas_PacketScheduler::UP_Lioumpas_PacketScheduler() :
		m_version(2), m_maxRBsForM2M(0.4) {
	if (ConfigParameters::Init()->GetMaxRBsForM2M() >= 0.0) {
		m_maxRBsForM2M = ConfigParameters::Init()->GetMaxRBsForM2M();
	}
}

UP_Lioumpas_PacketScheduler::~UP_Lioumpas_PacketScheduler() {
}

void UP_Lioumpas_PacketScheduler::RBsAllocation() {
	int nbRBs = GetMacEntity()->GetDevice()->GetPhy()->GetBandwidthManager()->GetUlSubChannels().size();
	int nbRBsForM2M = (int) floor(m_maxRBsForM2M * nbRBs);

	UpdateGrant();

	UsersToSchedule *users = GetUsersToSchedule();
	UsersToSchedule::iterator itUser;
	UsersToSchedule *m2mUsers = new UsersToSchedule[users->size()];
	UsersToSchedule *h2hUsers = new UsersToSchedule[users->size()];

	for (itUser = users->begin(); itUser != users->end(); itUser++) {
		UserToSchedule* user = *itUser;
		if (IsM2M(user)) {
			m2mUsers->push_back(user);
		} else {
			h2hUsers->push_back(user);
		}
	}
#ifdef M2M_SCHEDULER_DEBUG
	std::cout << "\n" << FrameManager::Init()->GetTTICounter() << " H2H " << h2hUsers->size() << " M2M "
	<< m2mUsers->size() << std::endl;
#endif

	int lastRB;
	if (m_version == 1) {
		lastRB = DoM2MScheduleV1(m2mUsers, 0, nbRBsForM2M);
	} else {
		lastRB = DoM2MScheduleV2(m2mUsers, 0, nbRBsForM2M);
	}

	// TODO melhorar o uso do RBs. Pode ficar RBs sem uso
	int rbStartForH2H = lastRB + 1;
	int nbRBsForH2H = nbRBs - rbStartForH2H;
	if (nbRBsForH2H > 0)
		DoH2HSchedule(h2hUsers, rbStartForH2H, nbRBsForH2H);

#ifdef M2M_SCHEDULER_DEBUG
	std::cout << "--" << std::endl;
#endif

	delete[] m2mUsers;
	delete[] h2hUsers;
}

int UP_Lioumpas_PacketScheduler::DoM2MScheduleV1(UsersToSchedule *usersToSchedule, int rbStart, int nbRBs) {
	std::vector<UserToSchedule*> users;
	std::vector<std::pair<int, std::pair<UserToSchedule*, int> > > metrics;
	std::map<UserToSchedule*, double> delays;
	int maxRBIndex = -1;

	for (int i = 0; i < usersToSchedule->size(); i++) {
		UserToSchedule* user = usersToSchedule->at(i);
		if (user != NULL && HasGrant(user)) {
			users.push_back(user);
			double delay = GetUpBearer(user)->GetQoSParameters()->GetMaxDelay();
			delays.insert(std::make_pair(user, delay));
			for (int rbI = 0; rbI < nbRBs; rbI++) {
				int cqi = user->m_channelContition.at(rbI + rbStart);
				std::pair<UserToSchedule *, int> id = std::make_pair(user, rbI);
				metrics.push_back(std::make_pair(cqi, id));
			}
		}
	}
	std::sort(metrics.begin(), metrics.end(), compareMetricsV1);

	int nbRBsAvailable = nbRBs;
	std::vector<bool> freeRBs;
	freeRBs.assign(nbRBs, true);

	std::vector<std::pair<int, std::pair<UserToSchedule *, int> > >::iterator itMetric;
	for (itMetric = metrics.begin(); itMetric != metrics.end() && nbRBsAvailable > 0; itMetric++) {
		UserToSchedule* user = (*itMetric).second.first;
		int userCQI = (*itMetric).first;
		std::map<UserToSchedule*, double>::iterator itUserDelay = delays.find(user);
		int rbIndex = (*itMetric).second.second;
		if (!freeRBs[rbIndex] || user->m_listOfAllocatedRBs.size() > 0 || itUserDelay == delays.end()) {
			continue;
		}
		double userDelay = itUserDelay->second;
		double meanDelay = 0.0;
		int delaySize = 0;

		std::map<UserToSchedule*, double>::iterator itDelay;
		for (itDelay = delays.begin(); itDelay != delays.end(); itDelay++) {
			meanDelay += itDelay->second;
			delaySize++;
		}

		if (delaySize == 0 || userDelay <= meanDelay / (2.0 * delaySize)) {
			std::pair<int, int> rbRange = std::make_pair(rbIndex, rbIndex);
			int rbRangeSize = 1;
			bool needsRBs = true;
			double specEffic;
			int mcs, tbs;

			do {
				rbRangeSize = rbRange.second - rbRange.first + 1;
				specEffic = GetSpectralEfficiency(user, rbRange.first + rbStart, rbRangeSize);
				mcs = GetMacEntity()->GetAmcModule()->GetMCSFromCQI(
						GetMacEntity()->GetAmcModule()->GetCQIFromSinr(specEffic));
				tbs = (GetMacEntity()->GetAmcModule()->GetTBSizeFromMCS(mcs, rbRangeSize)) / 8;

				if (tbs >= user->m_dataToTransmit || nbRBsAvailable - rbRangeSize <= 0) {
					needsRBs = false;
				} else {
					bool leftExpanded = false;
					if (rbRange.first > 0) {
						rbIndex = rbRange.first - 1;
						int maxCQI = 0;
						for (itDelay = delays.begin(); itDelay != delays.end(); itDelay++) {
							int cqi = itDelay->first->m_channelContition.at(rbIndex + rbStart);
							if (cqi >= maxCQI)
								maxCQI = cqi;
						}
						if (userCQI >= maxCQI) {
							leftExpanded = true;
							rbRange.first = rbIndex;
						}
					}

					bool rightExpanded = false;
					if (!leftExpanded) {
						if (rbRange.second < nbRBs - 1) {
							rbIndex = rbRange.second + 1;
							int maxCQI = 0;
							for (itDelay = delays.begin(); itDelay != delays.end(); itDelay++) {
								int cqi = itDelay->first->m_channelContition.at(rbIndex + rbStart);
								if (cqi >= maxCQI)
									maxCQI = cqi;
							}
							if (userCQI >= maxCQI) {
								rightExpanded = true;
								rbRange.second = rbIndex;
							}
						}
					}

					if (!leftExpanded && !rightExpanded)
						needsRBs = false;
				}
			} while (needsRBs && nbRBsAvailable - rbRangeSize > 0);

			user->m_selectedMCS = mcs;
			user->m_transmittedData = tbs;
			for (int i = rbRange.first; i <= rbRange.second; i++) {
				freeRBs[i] = false;
				user->m_listOfAllocatedRBs.push_back(i + rbStart);
				nbRBsAvailable--;
			}
			if (rbRange.second > maxRBIndex)
				maxRBIndex = rbRange.second;
			SetGrant(user, 0);

#ifdef M2M_SCHEDULER_DEBUG
			std::cout << "ID " << user->m_userToSchedule->GetIDNetworkNode() << " data " << user->m_dataToTransmit
			<< " tbs " << tbs << " rbStart " << user->m_listOfAllocatedRBs[0] << " rbSize "
			<< user->m_listOfAllocatedRBs.size() << std::endl;
#endif
		} else {
			SetGrant(user, (int) floor(1000.0 * userDelay / 2.0)); // s -> ms
		}
		delays.erase(itUserDelay);
	}
	return maxRBIndex;
}

int UP_Lioumpas_PacketScheduler::DoM2MScheduleV2(UsersToSchedule *usersToSchedule, int rbStart, int nbRBs) {
	std::vector<UserToSchedule*> users;
	std::vector<std::pair<UserToSchedule*, double> > metrics;
	int maxRBIndex = -1;

	for (int i = 0; i < usersToSchedule->size(); i++) {
		UserToSchedule* user = usersToSchedule->at(i);
		if (user != NULL) {
			users.push_back(user);
			metrics.push_back(std::make_pair(user, GetUpBearer(user)->GetQoSParameters()->GetMaxDelay()));
		}
	}

	int nbRBsAvailable = nbRBs;
	std::vector<bool> freeRBs;
	freeRBs.assign(nbRBs, true);

	std::sort(metrics.begin(), metrics.end(), compareMetricsV2);
	std::vector<std::pair<UserToSchedule*, double> >::iterator itMetric;
	for (itMetric = metrics.begin(); itMetric != metrics.end(); itMetric++) {
		UserToSchedule* user = itMetric->first;
		std::pair<int, int> rbRange = std::make_pair(nbRBs, nbRBs);
		bool needsRBs = true;

		while (needsRBs && nbRBsAvailable > 0) {
			int rbChosen = nbRBs;
			int maxCQI = -1;
			for (int i = 0; i < nbRBs; i++) {
				if (!freeRBs[i])
					continue;
				if ((rbRange.first != nbRBs && rbRange.second != nbRBs)
						&& (i + 1 < rbRange.first || i - 1 > rbRange.second)) {
					continue;
				}
				int cqi = user->m_channelContition.at(i + rbStart);
				if (cqi >= maxCQI) {
					maxCQI = cqi;
					rbChosen = i;
				}
			}

			if (rbChosen != nbRBs) {
				if (rbChosen < rbRange.first || rbRange.first == nbRBs) {
					rbRange.first = rbChosen;
				}
				if (rbChosen > rbRange.second || rbRange.second == nbRBs) {
					rbRange.second = rbChosen;
				}
				freeRBs[rbChosen] = false;
				nbRBsAvailable--;
				if (rbChosen > maxRBIndex)
					maxRBIndex = rbChosen;
			}

			int rbRangeSize = 0;
			if (rbRange.first != nbRBs && rbRange.second != nbRBs) {
				rbRangeSize = rbRange.second - rbRange.first + 1;
			}
			if (rbRangeSize > 0) {
				double specEffic = GetSpectralEfficiency(user, rbRange.first + rbStart, rbRangeSize);
				int mcs = GetMacEntity()->GetAmcModule()->GetMCSFromCQI(
						GetMacEntity()->GetAmcModule()->GetCQIFromSinr(specEffic));
				int tbs = (GetMacEntity()->GetAmcModule()->GetTBSizeFromMCS(mcs, rbRangeSize)) / 8;

				if (tbs >= user->m_dataToTransmit || nbRBsAvailable <= 0) {
					needsRBs = false;
					user->m_selectedMCS = mcs;
					user->m_transmittedData = tbs;
					for (int i = 0; i < rbRangeSize; i++) {
						user->m_listOfAllocatedRBs.push_back(i + rbRange.first + rbStart);
					}
#ifdef M2M_SCHEDULER_DEBUG
					std::cout << "ID " << user->m_userToSchedule->GetIDNetworkNode() << " data "
					<< user->m_dataToTransmit << " tbs " << tbs << " rbStart " << user->m_listOfAllocatedRBs[0]
					<< " rbSize " << user->m_listOfAllocatedRBs.size() << std::endl;
#endif
				}
			} else {
				needsRBs = false;
			}
		}

		if (nbRBsAvailable <= 0)
			break;
	}
	return maxRBIndex;
}
