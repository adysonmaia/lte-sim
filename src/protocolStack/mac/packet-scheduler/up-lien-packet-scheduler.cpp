/*
 * up-lien-packet-scheduler.cpp
 *
 *  Created on: 09/06/2014
 *      Author: adyson
 */

#include "up-lien-packet-scheduler.h"
#include "../mac-entity.h"
#include "../AMCModule.h"
#include "../../../core/eventScheduler/simulator.h"
#include "../../../core/spectrum/bandwidth-manager.h"
#include "../../../flows/radio-bearer.h"
#include "../../../flows/QoS/QoSParameters.h"
#include "../../../device/NetworkNode.h"
#include "../../../phy/lte-phy.h"
#include "../../../load-parameters.h"
#include "../../../componentManagers/FrameManager.h"
#include "cmath"
#include <algorithm>

static bool compareMetrics(const std::pair<double, UplinkPacketScheduler::UserToSchedule*> &i,
		const std::pair<double, UplinkPacketScheduler::UserToSchedule*> &j) {
	return (i.first < j.first);
}

int UP_Lien_PacketScheduler::GetAgti(double delay) {
	int agti = (int) floor(delay * 1000); // s -> ms
	return agti;
}

int UP_Lien_PacketScheduler::GetAgti(UserToSchedule *user) {
	RadioBearer *bearer = GetUpBearer(user);
	if (bearer == NULL)
		return 0;
	double delay = bearer->GetQoSParameters()->GetMaxDelay();
	return GetAgti(delay);
}

UP_Lien_PacketScheduler::UP_Lien_PacketScheduler() :
		m_rbsPerM2M(5) {
}

UP_Lien_PacketScheduler::~UP_Lien_PacketScheduler() {
}

bool UP_Lien_PacketScheduler::HasGrant(UserToSchedule *user) {
	unsigned long tti = FrameManager::Init()->GetTTICounter();
	int agti = GetAgti(user);
	int firstTti = tti;
	std::map<int, int>::iterator it = m_agtiFirstTTI.find(agti);
	if (it != m_agtiFirstTTI.end())
		firstTti = it->second;
	else
		m_agtiFirstTTI.insert(std::make_pair(agti, tti));
	int time = tti - firstTti;

//	std::cout << "id " << user->m_userToSchedule->GetIDNetworkNode() << " tti " << tti << " first " << firstTti
//			<< " time " << time <<  " agti " << agti << " time % agti " << (time % agti) << std::endl;

	return (agti == 0 || time <= 0 || time % agti == 0);
}

void UP_Lien_PacketScheduler::SetGrant(UserToSchedule *user, int time) {
	int agti = GetAgti(user);
	std::map<int, int>::iterator it = m_agtiFirstTTI.find(agti);
	if (it != m_agtiFirstTTI.end())
		it->second = time;
	else
		m_agtiFirstTTI.insert(std::make_pair(agti, time));
}

int UP_Lien_PacketScheduler::DoM2MSchedule(UsersToSchedule *users, int rbStart, int nbRBs) {
	std::vector<std::pair<double, UserToSchedule*> > metrics;
	int nbRBsAvailable = nbRBs;
	int maxRBIndex = -1;
	int firstRB = 0;
	int lastAgti = -1;

	for (UsersToSchedule::iterator it = users->begin(); it != users->end(); it++) {
		UserToSchedule *user = *it;
		if (HasGrant(user)) {
			RadioBearer *bearer = GetUpBearer(user);
			double delay = bearer->GetQoSParameters()->GetMaxDelay();
			metrics.push_back(std::make_pair(delay, user));
		}
	}
	std::sort(metrics.begin(), metrics.end(), compareMetrics);

	std::vector<std::pair<double, UserToSchedule*> >::iterator itMetric;
	for (itMetric = metrics.begin(); itMetric != metrics.end() && nbRBsAvailable > 0; itMetric++) {
		UserToSchedule *user = itMetric->second;
		if (user->m_listOfAllocatedRBs.size() > 0) {
			continue;
		}
		int rbSize = (this->m_rbsPerM2M < nbRBsAvailable) ? this->m_rbsPerM2M : nbRBsAvailable;

		double specEffic = GetSpectralEfficiency(user, firstRB + rbStart, rbSize);
		int mcs = GetMacEntity()->GetAmcModule()->GetMCSFromCQI(
				GetMacEntity()->GetAmcModule()->GetCQIFromSinr(specEffic));
		int tbs = (GetMacEntity()->GetAmcModule()->GetTBSizeFromMCS(mcs, rbSize)) / 8;
		user->m_selectedMCS = mcs;
		user->m_transmittedData = tbs;
		for (int i = firstRB; i < firstRB + rbSize; i++) {
			user->m_listOfAllocatedRBs.push_back(i + rbStart);
			nbRBsAvailable--;
			maxRBIndex = i;
		}
		firstRB += rbSize;
		lastAgti = GetAgti(itMetric->first);

#ifdef M2M_SCHEDULER_DEBUG
		std::cout << "ID " << user->m_userToSchedule->GetIDNetworkNode() << " data " << user->m_dataToTransmit
				<< " tbs " << tbs << " rbStart " << user->m_listOfAllocatedRBs[0] << " rbSize "
				<< user->m_listOfAllocatedRBs.size() << " metric " << itMetric->first << std::endl;
#endif
	}

	for (itMetric = metrics.begin(); itMetric != metrics.end() && lastAgti > 0; itMetric++) {
		UserToSchedule *user = itMetric->second;
		int agti = GetAgti(itMetric->first);
		if (user->m_listOfAllocatedRBs.size() == 0 && agti != lastAgti) {
			SetGrant(itMetric->second, FrameManager::Init()->GetTTICounter() + 1);
		}
	}

	return (maxRBIndex + rbStart);
}

int UP_Lien_PacketScheduler::DoH2HSchedule(UsersToSchedule *users, int rbStart, int nbRBs) {
	return UP_PF_PacketScheduler::DoH2HSchedule(users, rbStart, nbRBs);
}

