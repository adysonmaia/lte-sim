/*
 * up-abdalla-packet-scheduler.cpp
 *
 *  Created on: 13/05/2014
 *      Author: adyson
 */

#include "up-abdalla-packet-scheduler.h"
#include "../mac-entity.h"
#include "../../../flows/radio-bearer.h"
#include "../../../flows/QoS/QoSParameters.h"
#include "../../../device/NetworkNode.h"
#include "../../../phy/lte-phy.h"
#include "../../../utility/eesm-effective-sinr.h"
#include "../../../core/spectrum/bandwidth-manager.h"
#include "../../../componentManagers/FrameManager.h"
#include "../../../utility/ConfigParameters.h"
#include "../../../load-parameters.h"
#include "../AMCModule.h"
#include <cmath>
#include <algorithm>
#include <vector>

static bool compareMetrics(const std::pair<double, UplinkPacketScheduler::UserToSchedule*> &i,
		const std::pair<double, UplinkPacketScheduler::UserToSchedule*> &j) {
	return (i.first < j.first);
}

UP_Abdalla_PacketScheduler::UP_Abdalla_PacketScheduler() :
		m_maxRBsForM2M(0.4) {
	if (ConfigParameters::Init()->GetMaxRBsForM2M() >= 0.0) {
		m_maxRBsForM2M = ConfigParameters::Init()->GetMaxRBsForM2M();
	}
}

UP_Abdalla_PacketScheduler::~UP_Abdalla_PacketScheduler() {
}

double UP_Abdalla_PacketScheduler::ComputeSchedulingMetric(UserToSchedule *user) {
	double metric = GetUpBearer(user)->GetQoSParameters()->GetMaxDelay();
	return metric;
}

void UP_Abdalla_PacketScheduler::RBsAllocation() {
	int nbRBs = GetMacEntity()->GetDevice()->GetPhy()->GetBandwidthManager()->GetUlSubChannels().size();
	int nbRBsForM2M = (int) floor(m_maxRBsForM2M * nbRBs);

	UsersToSchedule *users = GetUsersToSchedule();
	UsersToSchedule::iterator itUser;
	UsersToSchedule *m2mUsers = new UsersToSchedule[users->size()];
	UsersToSchedule *h2hUsers = new UsersToSchedule[users->size()];

	for (itUser = users->begin(); itUser != users->end(); itUser++) {
		UserToSchedule* user = *itUser;
		if (IsM2M(user)) {
			m2mUsers->push_back(user);
		} else {
			h2hUsers->push_back(user);
		}
	}

#ifdef M2M_SCHEDULER_DEBUG
	std::cout << "\n" << FrameManager::Init()->GetTTICounter() << " H2H " << h2hUsers->size() << " M2M "
	<< m2mUsers->size() << std::endl;
#endif

	int lastRB = DoM2MSchedule(m2mUsers, 0, nbRBsForM2M);
	int rbStartForH2H = lastRB + 1;
	int nbRBsForH2H = nbRBs - rbStartForH2H;
	if (nbRBsForH2H > 0)
		DoH2HSchedule(h2hUsers, rbStartForH2H, nbRBsForH2H);

#ifdef M2M_SCHEDULER_DEBUG
	std::cout << "--" << std::endl;
#endif

	delete[] m2mUsers;
	delete[] h2hUsers;
}

int UP_Abdalla_PacketScheduler::DoH2HSchedule(UsersToSchedule *users, int rbStart, int nbRBs) {
	return DoM2MSchedule(users, rbStart, nbRBs);
}

int UP_Abdalla_PacketScheduler::DoM2MSchedule(UsersToSchedule *users, int rbStart, int nbRBs) {
	std::vector<std::pair<double, UserToSchedule*> > metrics;
	std::vector<std::pair<double, UserToSchedule*> >::iterator itMetric;
	UsersToSchedule::iterator itUser;
	for (itUser = users->begin(); itUser != users->end(); itUser++) {
		double metric = ComputeSchedulingMetric(*itUser);
		metrics.push_back(std::make_pair(metric, *itUser));
	}

	std::sort(metrics.begin(), metrics.end(), compareMetrics);

	int nbRBsAvailable = nbRBs;
	int lastRB = 0;
	int maxRBIndex = -1;
	for (itMetric = metrics.begin(); itMetric != metrics.end() && nbRBsAvailable > 0; itMetric++) {
		UserToSchedule *user = itMetric->second;
		if (user->m_listOfAllocatedRBs.size() > 0)
			continue;

		std::pair<int, int> rbRange = std::make_pair(lastRB, lastRB);
		int rbRangeSize = 1;
		double specEffic;
		int mcs, tbs;
		bool found = false;
		for (int i = lastRB; i < nbRBs; i++) {
			rbRangeSize = i - rbRange.first + 1;
			specEffic = GetSpectralEfficiency(user, rbRange.first + rbStart, rbRangeSize);
			mcs = GetMacEntity()->GetAmcModule()->GetMCSFromCQI(
					GetMacEntity()->GetAmcModule()->GetCQIFromSinr(specEffic));
			tbs = (GetMacEntity()->GetAmcModule()->GetTBSizeFromMCS(mcs, rbRangeSize)) / 8;
			if (tbs >= user->m_dataToTransmit || nbRBsAvailable - rbRangeSize <= 0) {
				rbRange.second = i;
				lastRB = i + 1;
				found = true;
				break;
			}
		}

		if (found) {
			user->m_selectedMCS = mcs;
			user->m_transmittedData = tbs;
			for (int i = rbRange.first; i <= rbRange.second; i++) {
				user->m_listOfAllocatedRBs.push_back(i + rbStart);
				nbRBsAvailable--;
			}
			if (rbRange.second > maxRBIndex)
				maxRBIndex = rbRange.second;

#ifdef M2M_SCHEDULER_DEBUG
			std::cout << "ID " << user->m_userToSchedule->GetIDNetworkNode() << " data " << user->m_dataToTransmit
			<< " tbs " << tbs << " rbStart " << user->m_listOfAllocatedRBs[0] << " rbSize "
			<< user->m_listOfAllocatedRBs.size() << " metric " << itMetric->first << std::endl;
#endif
		}
	}
	return (maxRBIndex + rbStart);
}
