/*
 * up-afrin-packet-scheduler.cpp
 *
 *  Created on: 13/05/2014
 *      Author: adyson
 */

#include "up-afrin-packet-scheduler.h"
#include "../mac-entity.h"
#include "../../../core/eventScheduler/simulator.h"
#include "../../../flows/radio-bearer.h"
#include "../../../flows/QoS/QoSParameters.h"
#include "../../../device/NetworkNode.h"
#include "../../../load-parameters.h"

UP_Afrin_PacketScheduler::UP_Afrin_PacketScheduler() :
		m_maxDataToTransmit(0) {
}

UP_Afrin_PacketScheduler::~UP_Afrin_PacketScheduler() {
}

double UP_Afrin_PacketScheduler::ComputeSchedulingMetric(UserToSchedule *user) {
	double delay = GetUpBearer(user)->GetQoSParameters()->GetMaxDelay();
	double deadline = delay + user->m_lastRequestTime - Simulator::Init()->Now();
	deadline *= 1000.0; // s -> ms
	double metric = 1.0;
	if (deadline > 1.0 && m_maxDataToTransmit > 0) {
		metric = ((double) user->m_dataToTransmit / (double) m_maxDataToTransmit) * (1.0 / deadline);
	}
	return (1.0 - metric); // sort is in ascending order, so it inverts the value
}

int UP_Afrin_PacketScheduler::DoH2HSchedule(UsersToSchedule *users, int rbStart, int nbRBs) {
	return UP_PF_PacketScheduler::DoH2HSchedule(users, rbStart, nbRBs);
}

int UP_Afrin_PacketScheduler::DoM2MSchedule(UsersToSchedule *users, int rbStart, int nbRBs) {
	m_maxDataToTransmit = 0;
	for (UsersToSchedule::iterator it = users->begin(); it != users->end(); it++) {
		if ((*it)->m_dataToTransmit > m_maxDataToTransmit)
			m_maxDataToTransmit = (*it)->m_dataToTransmit;
	}
	return UP_Abdalla_PacketScheduler::DoM2MSchedule(users, rbStart, nbRBs);
}

