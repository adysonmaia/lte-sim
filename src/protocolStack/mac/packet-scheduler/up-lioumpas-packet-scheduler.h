/*
 * up-lioumpas-packetS-scheduler.h
 *
 *  Created on: 10/05/2014
 *      Author: adyson
 */

#ifndef UP_LIOUMPAS_PACKETS_SCHEDULER_H_
#define UP_LIOUMPAS_PACKETS_SCHEDULER_H_

#include "up-pf-packet-scheduler.h"
#include <utility>
#include <map>

class UP_Lioumpas_PacketScheduler: public UP_PF_PacketScheduler {
public:
	UP_Lioumpas_PacketScheduler();
	virtual ~UP_Lioumpas_PacketScheduler();

	virtual void RBsAllocation();

	inline void SetVersion(int version) {
		this->m_version = version;
	}
	inline int GetVersion() const {
		return this->m_version;
	}
	inline void SetMaxRBsForM2M(double value) {
		this->m_maxRBsForM2M = value;
	}
	inline double GetMaxRBsForM2M() const {
		return this->m_maxRBsForM2M;
	}
protected:
	int DoM2MScheduleV1(UsersToSchedule *usersToSchedule, int rbStart, int nbRBs);
	int DoM2MScheduleV2(UsersToSchedule *usersToSchedule, int rbStart, int nbRBs);

	int m_version;
	double m_maxRBsForM2M;
};

#endif /* UP_LIOUMPAS_PACKETS_SCHEDULER_H_ */
