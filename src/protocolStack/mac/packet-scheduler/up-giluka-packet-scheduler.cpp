/*
 * up-giluka-packet-scheduler.cpp
 *
 *  Created on: 13/05/2014
 *      Author: adyson
 */

#include "up-giluka-packet-scheduler.h"
#include "../mac-entity.h"
#include "../AMCModule.h"
#include "../../../flows/radio-bearer.h"
#include "../../../flows/QoS/QoSParameters.h"
#include "../../../device/NetworkNode.h"
#include "../../../phy/lte-phy.h"
#include "../../../utility/eesm-effective-sinr.h"
#include "../../../core/spectrum/bandwidth-manager.h"
#include "../../../componentManagers/FrameManager.h"
#include "../../../core/eventScheduler/simulator.h"
#include "../../../load-parameters.h"
#include <cmath>
#include <algorithm>

#define NUMBER_CLASSES 6
#define DELAY_CLASS_1 50.0 // ms
#define DELAY_CLASS_2 100.0
#define DELAY_CLASS_3 150.0
#define DELAY_CLASS_4 300.0
#define DELAY_CLASS_5 500.0
#define DELAY_CLASS_6 0.0

//static bool compareMetrics(const std::pair<double, UplinkPacketScheduler::UserToSchedule*> &i,
//		const std::pair<double, UplinkPacketScheduler::UserToSchedule*> &j) {
//	return (i.first < j.first);
//}

UP_Giluka_PacketScheduler::UP_Giluka_PacketScheduler() {
}

UP_Giluka_PacketScheduler::~UP_Giluka_PacketScheduler() {
}

void UP_Giluka_PacketScheduler::RBsAllocation() {
	double classDelays[] = { DELAY_CLASS_1, DELAY_CLASS_2, DELAY_CLASS_3, DELAY_CLASS_4, DELAY_CLASS_5, DELAY_CLASS_6 };

	std::vector<std::vector<std::pair<double, UserToSchedule*> > > m2mClasses, h2hClasses;
	std::vector<std::vector<std::pair<double, UserToSchedule*> > > *ptClass;
	m2mClasses.assign(NUMBER_CLASSES, std::vector<std::pair<double, UserToSchedule*> >());
	h2hClasses.assign(NUMBER_CLASSES, std::vector<std::pair<double, UserToSchedule*> >());

	double now = Simulator::Init()->Now();
	UsersToSchedule *users = GetUsersToSchedule();
	for (UsersToSchedule::iterator itUser = users->begin(); itUser != users->end(); itUser++) {
		UserToSchedule *user = *itUser;
		double metric = 1000.0 * (GetUpBearer(user)->GetQoSParameters()->GetMaxDelay() - now + user->m_lastRequestTime);
		if (metric < 0.0)
			metric = 0.0;
		if (IsM2M(user))
			ptClass = &m2mClasses;
		else
			ptClass = &h2hClasses;

		for (int i = 0; i < NUMBER_CLASSES; i++) {
			double delayLimit = classDelays[i];
			if (delayLimit <= 0.0 || metric <= delayLimit) {
				ptClass->at(i).push_back(std::make_pair(metric, user));
				break;
			}
		}
	}

	int nbRBs = GetMacEntity()->GetDevice()->GetPhy()->GetBandwidthManager()->GetUlSubChannels().size();
	int lastRB = -1;
	int nbRBsAvailable = nbRBs;

#ifdef M2M_SCHEDULER_DEBUG
	std::cout << "\n" << FrameManager::Init()->GetTTICounter() << std::endl;
#endif

	for (int i = 0; i < NUMBER_CLASSES; i++) {
		for (int j = 0; j < 2; j++) {
			if (j == 0)
				ptClass = &h2hClasses;
			else
				ptClass = &m2mClasses;

			lastRB++;
			nbRBsAvailable = nbRBs - lastRB;

			std::vector<std::pair<double, UserToSchedule*> > *users = &(ptClass->at(i));

#ifdef M2M_SCHEDULER_DEBUG
			std::cout << "\tClass (" << i << " , " << (j == 0 ? "H2H" : "M2M") << ")  size " << users->size()
			<< std::endl;
#endif

			if (nbRBsAvailable > 0 && users->size() > 0) {
//				std::sort(users->begin(), users->end(), compareMetrics); // TODO the algorithm has that?
				lastRB = DoClassSchedule(users, lastRB, nbRBsAvailable);
			}
		}
	}

#ifdef M2M_SCHEDULER_DEBUG
	std::cout << "--" << std::endl;
#endif
}

int UP_Giluka_PacketScheduler::DoClassSchedule(std::vector<std::pair<double, UserToSchedule*> > *users, int rbStart,
		int nbRBs) {
	std::vector<std::pair<double, UserToSchedule*> >::iterator itUser;
	int nbRBsAvailable = nbRBs;
	int lastRB = 0;
	int maxRBIndex = -1;
	for (itUser = users->begin(); itUser != users->end() && nbRBsAvailable > 0; itUser++) {
		UserToSchedule *user = itUser->second;
		if (user->m_listOfAllocatedRBs.size() > 0) {
			continue;
		}

		std::pair<int, int> rbRange = std::make_pair(lastRB, lastRB);
		int rbRangeSize = 1;
		double specEffic;
		int mcs, tbs;
		bool found = false;
		for (int i = lastRB; i < nbRBs; i++) {
			rbRangeSize = i - rbRange.first + 1;
			specEffic = GetSpectralEfficiency(user, rbRange.first + rbStart, rbRangeSize);
			mcs = GetMacEntity()->GetAmcModule()->GetMCSFromCQI(
					GetMacEntity()->GetAmcModule()->GetCQIFromSinr(specEffic));
			tbs = (GetMacEntity()->GetAmcModule()->GetTBSizeFromMCS(mcs, rbRangeSize)) / 8;
			if (tbs >= user->m_dataToTransmit || nbRBsAvailable - rbRangeSize <= 0) {
				rbRange.second = i;
				lastRB = i + 1;
				found = true;
				break;
			}
		}

		if (found) {
			user->m_selectedMCS = mcs;
			user->m_transmittedData = tbs;
			for (int i = rbRange.first; i <= rbRange.second; i++) {
				user->m_listOfAllocatedRBs.push_back(i + rbStart);
				nbRBsAvailable--;
			}
			if (rbRange.second > maxRBIndex)
				maxRBIndex = rbRange.second;

#ifdef M2M_SCHEDULER_DEBUG
			std::cout << "ID " << user->m_userToSchedule->GetIDNetworkNode() << " data " << user->m_dataToTransmit
			<< " tbs " << tbs << " rbStart " << user->m_listOfAllocatedRBs[0] << " rbSize "
			<< user->m_listOfAllocatedRBs.size() << " metric " << itUser->first << std::endl;
#endif
		}
	}
	return (maxRBIndex + rbStart);
}
