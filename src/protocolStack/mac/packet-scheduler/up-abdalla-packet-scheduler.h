/*
 * up-abdalla-packet-scheduler.h
 *
 *  Created on: 13/05/2014
 *      Author: adyson
 */

#ifndef UP_ABDALLA_PACKET_SCHEDULER_H_
#define UP_ABDALLA_PACKET_SCHEDULER_H_

#include "up-pf-packet-scheduler.h"

class UP_Abdalla_PacketScheduler: public UP_PF_PacketScheduler {
public:
	UP_Abdalla_PacketScheduler();
	virtual ~UP_Abdalla_PacketScheduler();

	virtual void RBsAllocation();
	inline void SetMaxRBsForM2M(double value) {
		this->m_maxRBsForM2M = value;
	}
	inline double GetMaxRBsForM2M() const {
		return this->m_maxRBsForM2M;
	}
protected:
	using UP_PF_PacketScheduler::ComputeSchedulingMetric;
	virtual double ComputeSchedulingMetric(UserToSchedule *user);
	virtual int DoH2HSchedule(UsersToSchedule *users, int rbStart, int nbRBs);
	virtual int DoM2MSchedule(UsersToSchedule *users, int rbStart, int nbRBs);

	double m_maxRBsForM2M;
};

#endif /* UP_ABDALLA_PACKET_SCHEDULER_H_ */
