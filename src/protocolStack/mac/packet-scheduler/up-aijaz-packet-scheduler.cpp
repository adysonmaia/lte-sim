/*
 * up-aijaz-packet-scheduler.cpp
 *
 *  Created on: 12/06/2014
 *      Author: adyson
 */

#include "up-aijaz-packet-scheduler.h"
#include "../mac-entity.h"
#include "../AMCModule.h"
#include "../../../flows/radio-bearer.h"
#include "../../../flows/QoS/QoSParameters.h"
#include "../../../device/NetworkNode.h"
#include "../../../phy/lte-phy.h"
#include "../../../utility/eesm-effective-sinr.h"
#include "../../../core/spectrum/bandwidth-manager.h"
#include "../../../componentManagers/FrameManager.h"
#include "../../../utility/ConfigParameters.h"
#include "../../../load-parameters.h"
#include <cmath>
#include <algorithm>
#include <vector>

static bool compareMetricsV2(const std::pair<double, UplinkPacketScheduler::UserToSchedule*> &i,
		const std::pair<double, UplinkPacketScheduler::UserToSchedule*> &j) {
	return (i.first < j.first);
}

static bool compareMetricsV1(const std::pair<int, std::pair<UplinkPacketScheduler::UserToSchedule*, int> > &i,
		const std::pair<int, std::pair<UplinkPacketScheduler::UserToSchedule*, int> > &j) {
	return (i.first > j.first);
}

UP_Aijaz_PacketScheduler::UP_Aijaz_PacketScheduler() :
		m_version(1), m_maxRBsForM2M(0.4) {

}

UP_Aijaz_PacketScheduler::~UP_Aijaz_PacketScheduler() {
}

int UP_Aijaz_PacketScheduler::ComputeTBSize(UserToSchedule *user, int rbStart, int nbRBs) {
	double spectralEfficiency = GetSpectralEfficiency(user, rbStart, nbRBs);
	int mcs = GetMacEntity()->GetAmcModule()->GetMCSFromCQI(
			GetMacEntity()->GetAmcModule()->GetCQIFromSinr(spectralEfficiency));
	int tbs = (GetMacEntity()->GetAmcModule()->GetTBSizeFromMCS(mcs, nbRBs)) / 8;
	return tbs;
}

double UP_Aijaz_PacketScheduler::ComputeMeanDelay(UsersToSchedule *users, UserToSchedule *userException) {
	double meanDelay = 0.0;
	int count = 0;
	UsersToSchedule::iterator itUser;
	if (users != NULL) {
		for (itUser = users->begin(); itUser != users->end(); itUser++) {
			UserToSchedule* user = *itUser;
			if (user->m_listOfAllocatedRBs.size() == 0
					&& (userException == NULL
							|| user->m_userToSchedule->GetIDNetworkNode()
									!= userException->m_userToSchedule->GetIDNetworkNode())) {
				meanDelay += GetUpBearer(user)->GetQoSParameters()->GetMaxDelay();
				count++;
			}
		}
	}

	if (count > 0)
		meanDelay /= (double) count;
	return meanDelay;
}

void UP_Aijaz_PacketScheduler::RBsAllocation() {
	int nbRBs = GetMacEntity()->GetDevice()->GetPhy()->GetBandwidthManager()->GetUlSubChannels().size();
	UsersToSchedule *users = GetUsersToSchedule();
	UsersToSchedule::iterator itUser;
	UsersToSchedule *m2mUsers = new UsersToSchedule();
	UsersToSchedule *h2hUsers = new UsersToSchedule();

	for (itUser = users->begin(); itUser != users->end(); itUser++) {
		UserToSchedule* user = *itUser;
		if (IsM2M(user)) {
			m2mUsers->push_back(user);
		} else {
			h2hUsers->push_back(user);
		}
	}
#ifdef M2M_SCHEDULER_DEBUG
	std::cout << "\n" << FrameManager::Init()->GetTTICounter() << " H2H " << h2hUsers->size() << " M2M "
			<< m2mUsers->size() << std::endl;
#endif

	if (m_version == 1) {
		DoScheduleV1(users, m2mUsers, 0, nbRBs);
	} else {
		int nbRBsForM2M = (int) floor(m_maxRBsForM2M * nbRBs);
		int lastRB = DoM2MSchedule(m2mUsers, 0, nbRBsForM2M);
		int rbStartForH2H = lastRB + 1;
		int nbRBsForH2H = nbRBs - rbStartForH2H;
		if (nbRBsForH2H > 0) {
			DoH2HSchedule(h2hUsers, rbStartForH2H, nbRBsForH2H);
		}
	}

#ifdef M2M_SCHEDULER_DEBUG
	std::cout << "--" << std::endl;
#endif
	delete m2mUsers;
	delete h2hUsers;
}

int UP_Aijaz_PacketScheduler::DoScheduleV1(UsersToSchedule *allUsers, UsersToSchedule *m2mUsers, int rbStart,
		int nbRBs) {
	std::vector<std::pair<int, std::pair<UserToSchedule*, int> > > metrics;

	UsersToSchedule::iterator itUser;
	for (itUser = allUsers->begin(); itUser != allUsers->end(); itUser++) {
		UserToSchedule* user = *itUser;
		for (int rbI = 0; rbI < nbRBs; rbI++) {
			int cqi = user->m_channelContition.at(rbI + rbStart);
			std::pair<UserToSchedule *, int> id = std::make_pair(user, rbI);
			metrics.push_back(std::make_pair(cqi, id));
		}

	}
	std::sort(metrics.begin(), metrics.end(), compareMetricsV1);
	UsersToSchedule *sortedUsers = new UsersToSchedule();
	std::vector<std::pair<int, std::pair<UserToSchedule *, int> > >::iterator itMetric;
	for (itMetric = metrics.begin(); itMetric != metrics.end(); itMetric++) {
		UserToSchedule* user = (*itMetric).second.first;
		sortedUsers->push_back(user);
	}

	int maxRBIndex = DoBodySchedule(sortedUsers, m2mUsers, rbStart, nbRBs);

	delete sortedUsers;
	return maxRBIndex;
}

int UP_Aijaz_PacketScheduler::DoM2MSchedule(UsersToSchedule *users, int rbStart, int nbRBs) {
	std::vector<std::pair<double, UserToSchedule*> > metrics;

	UsersToSchedule::iterator itUser;
	for (itUser = users->begin(); itUser != users->end(); itUser++) {
		UserToSchedule* user = *itUser;
		double delay = GetUpBearer(user)->GetQoSParameters()->GetMaxDelay();
		metrics.push_back(std::make_pair(delay, user));
	}
	std::sort(metrics.begin(), metrics.end(), compareMetricsV2);
	UsersToSchedule *sortedUsers = new UsersToSchedule();
	std::vector<std::pair<double, UserToSchedule*> >::iterator itMetric;
	for (itMetric = metrics.begin(); itMetric != metrics.end(); itMetric++) {
		UserToSchedule* user = (*itMetric).second;
		sortedUsers->push_back(user);
	}

	int maxRBIndex = DoBodySchedule(sortedUsers, sortedUsers, rbStart, nbRBs);

	delete sortedUsers;
	return maxRBIndex;
}

int UP_Aijaz_PacketScheduler::DoH2HSchedule(UsersToSchedule *users, int rbStart, int nbRBs) {
	return DoScheduleV1(users, NULL, rbStart, nbRBs);
}

int UP_Aijaz_PacketScheduler::DoBodySchedule(UsersToSchedule *allUsers, UsersToSchedule *m2mUsers, int rbStart,
		int nbRBs) {
	int maxRBIndex = -1;
	if (allUsers == NULL || allUsers->size() == 0 || nbRBs == 0)
		return (maxRBIndex + rbStart);

	ChunkMap *chunks = new ChunkMap(nbRBs);
	int nbChunks = chunks->GetChunksSize();
	int nbRBsAvailable = nbRBs;

	UsersToSchedule::iterator itUser;
	for (itUser = allUsers->begin(); itUser != allUsers->end() && nbRBsAvailable > 0; itUser++) {
		UserToSchedule* user = *itUser;
		if (user == NULL && user->m_listOfAllocatedRBs.size() > 0) {
			continue;
		}
		bool isM2M = IsM2M(user);
		double meanDelay = 0.0;
		double delay = 0.0;
		if (isM2M) {
			meanDelay = ComputeMeanDelay(m2mUsers, user);
			delay = GetUpBearer(user)->GetQoSParameters()->GetMaxDelay();
		}
		double minPower = -1;
		int selectedIndex = -1;
		for (int chunkIndex = 0; chunkIndex < nbChunks; chunkIndex++) {
			struct ChunkMap::Chunk_s chunk = chunks->GetChunk(chunkIndex);
			if (!chunk.available)
				continue;
			double power = ComputePower(user, chunk.size);
			if (minPower <= 0.0 || power < minPower) {
				bool respectConstraint = false;
				if (isM2M) {
					respectConstraint = (meanDelay <= 0.0 || delay <= meanDelay);
				} else {
					int tbs = ComputeTBSize(user, chunk.start + rbStart, chunk.size);
					respectConstraint = (tbs >= user->m_dataToTransmit || chunk.size == nbRBsAvailable);
				}
				if (respectConstraint) {
					minPower = power;
					selectedIndex = chunkIndex;
				}
			}
		}
		if (selectedIndex >= 0) {
			struct ChunkMap::Chunk_s chunk = chunks->GetChunk(selectedIndex);
			double spectralEfficiency = GetSpectralEfficiency(user, chunk.start + rbStart, chunk.size);
			int mcs = GetMacEntity()->GetAmcModule()->GetMCSFromCQI(
					GetMacEntity()->GetAmcModule()->GetCQIFromSinr(spectralEfficiency));
			int tbs = (GetMacEntity()->GetAmcModule()->GetTBSizeFromMCS(mcs, chunk.size)) / 8;

			user->m_selectedMCS = mcs;
			user->m_transmittedData = tbs;
			for (int i = 0; i < chunk.size; i++) {
				user->m_listOfAllocatedRBs.push_back(i + chunk.start + rbStart);
			}

			nbRBsAvailable -= chunk.size;
			chunks->AllocateChunk(chunk.id);

			if (chunk.start + chunk.size - 1 > maxRBIndex) {
				maxRBIndex = chunk.start + chunk.size - 1;
			}

#ifdef M2M_SCHEDULER_DEBUG
			std::cout << "ID " << user->m_userToSchedule->GetIDNetworkNode() << " data " << user->m_dataToTransmit
					<< " tbs " << tbs << " rbStart " << user->m_listOfAllocatedRBs[0] << " rbSize "
					<< user->m_listOfAllocatedRBs.size() << std::endl;
#endif
		}
	}

	delete chunks;
	return (maxRBIndex + rbStart);
}

