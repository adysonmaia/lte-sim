/*
 * up-pf-packet-scheduler.h
 *
 *  Created on: 06/05/2014
 *      Author: adyson
 */

#ifndef UP_PF_PACKET_SCHEDULER_H_
#define UP_PF_PACKET_SCHEDULER_H_

#include "uplink-packet-scheduler.h"
#include <map>
#include <vector>

typedef struct {
	int lastTransmittedBytes;
	int lastReceivedBytes;
	int lastRBsAllocated;
	int totalTransmittedBytes;
	double averageTransmissionRate;
	double averageRBsAllocated;
	double averageReceivedBytes;
} UP_PS_Statistic;

class UP_PF_PacketScheduler: public UplinkPacketScheduler {
public:
	UP_PF_PacketScheduler();
	virtual ~UP_PF_PacketScheduler();
	virtual void RBsAllocation();

	virtual double ComputeSchedulingMetric(RadioBearer *bearer, double spectralEfficiency, int subChannel);
	virtual double ComputeSchedulingMetric(UserToSchedule* user, int rbStart, int nbRBs);
	virtual double ComputeSchedulingMetric(UserToSchedule* user, int nbSubChannels);
	virtual double ComputeSchedulingMetric(UserToSchedule* user, double spectralEfficiency, int nbSubChannels);
protected:
	virtual int DoH2HSchedule(UsersToSchedule *usersToSchedule, int rbStart, int nbRBs);
	UP_PS_Statistic* GetStatistic(UserToSchedule* user);
	RadioBearer* GetUpBearer(UserToSchedule *user);
	void UpdateStatistic();
	double GetSpectralEfficiency(UserToSchedule* user, int rbStart, int rbSize);
	bool IsM2M(UserToSchedule *user);
	virtual bool HasGrant(UserToSchedule *user);
	virtual void UpdateGrant();
	virtual void SetGrant(UserToSchedule *user, int time);
	virtual int GetGrant(UserToSchedule *user);

	std::map<int, UP_PS_Statistic> m_statistic;
	std::map<int, int> m_grantTimer;
};

class ChunkMap {
public:
	struct Chunk_s {
		int id;
		int start;
		int size;
		bool available;
	};

	ChunkMap(int nbRBs);
	virtual ~ChunkMap();
	inline struct Chunk_s GetChunk(int id) {
		return this->chunks[id];
	}
	struct Chunk_s GetChunk(int start, int size);
	void AllocateChunk(int key);
	inline int GetChunksSize() {
		return this->nbChunks;
	}
private:
	int nbRBs;
	int nbChunks;
	struct Chunk_s *chunks;
};

class RBsAllocationMap {
public:
	struct RBsMapValue_s {
		bool allocated;
		int ueID;
	};

	RBsAllocationMap(const int nbRBs);
	~RBsAllocationMap();

	bool IsAvailable(const int indexStart, const int size = 1) const;
	void Allocate(const int ueID, const int indexStart, const int size = 1);
	std::vector<int> GetIndexes(const int ueID) const;
	bool HasResources(const int ueID) const;
	int GetUeID(const int index) const;
	int GetSize() const;
	int GetAvailableRBsSize() const;
	int GetFirstAvailableRB() const;
	void Clear();
private:
	std::vector<struct RBsMapValue_s> m_map;
};

#endif /* UP_PF_PACKET_SCHEDULER_H_ */
