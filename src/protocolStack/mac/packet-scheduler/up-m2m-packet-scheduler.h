/*
 * up-m2m-packet-scheduler.h
 *
 *  Created on: 22/07/2014
 *      Author: adyson
 */

#ifndef UP_M2M_PACKET_SCHEDULER_H_
#define UP_M2M_PACKET_SCHEDULER_H_

#include "up-pf-packet-scheduler.h"

class UP_M2M_PacketScheduler: public UP_PF_PacketScheduler {
public:
	UP_M2M_PacketScheduler();
	virtual ~UP_M2M_PacketScheduler();

	virtual void RBsAllocation();

	inline void SetVersion(int version) {
		this->m_version = version;
	}
	inline int GetVersion() const {
		return this->m_version;
	}
	inline void SetMinRBsForM2M(int value) {
		this->m_minRBsM2M = value;
	}
	inline int GetMinRBsForM2M() const {
		return this->m_minRBsM2M;
	}
	inline void SetMinRBsForH2H(int value) {
		this->m_minRBsH2H = value;
	}
	inline int GetMinRBsForH2H() const {
		return this->m_minRBsH2H;
	}
	inline void SetMinPercentRBsForM2M(double value) {
		this->m_minPercentRBsM2M = value;
	}
	inline double GetMinPercentRBsForM2M() {
		return this->m_minPercentRBsM2M;
	}
	inline void SetDeadlineWeight(double value) {
		this->m_deadlineMetricWeight = value;
	}
	inline double GetDeadlineWeight() {
		return this->m_deadlineMetricWeight;
	}
	inline double GetRequestDeniedSpread() const {
		return this->m_requestDeniedSpread;
	}
	inline void SetRequestDeniedSpread(double requestDeniedSpread) {
		this->m_requestDeniedSpread = requestDeniedSpread;
	}
	inline double GetCongestionHigh() const {
		return m_congestionHigh;
	}
	inline void SetCongestionHigh(double congestionHigh) {
		m_congestionHigh = congestionHigh;
	}
	inline double GetCongestionLow() const {
		return m_congestionLow;
	}
	inline void SetCongestionLow(double congestionLow) {
		m_congestionLow = congestionLow;
	}
	inline double GetCongestionTimeWindow() const {
		return m_congestionTimeWindow;
	}
	inline void SetCongestionTimeWindow(double congestionTimeWindow) {
		m_congestionTimeWindow = congestionTimeWindow;
	}
	inline double GetMinPercentRBsM2MHigh() const {
		return m_minPercentRBsM2MHigh;
	}
	inline void SetMinPercentRBsM2MHigh(double minPercentRBsM2MHigh) {
		m_minPercentRBsM2MHigh = minPercentRBsM2MHigh;
	}
	inline double GetMinPercentRBsM2MLow() const {
		return m_minPercentRBsM2MLow;
	}
	inline void SetMinPercentRBsM2MLow(double minPercentRBsM2MLow) {
		m_minPercentRBsM2MLow = minPercentRBsM2MLow;
	}
	inline double GetMinPercentRBsM2MNormal() const {
		return m_minPercentRBsM2MNormal;
	}
	inline void SetMinPercentRBsM2MNormal(double minPercentRBsM2MNormal) {
		m_minPercentRBsM2MNormal = minPercentRBsM2MNormal;
	}
protected:
	int DoM2MSchedule(UsersToSchedule *usersToSchedule, int rbStart, int nbRBs);
	double GetDeadline(UserToSchedule* user);
	void UpdateCongestion(UsersToSchedule *users);

	int m_version;
	int m_minRBsH2H;
	int m_minRBsM2M;
	double m_minPercentRBsM2M;
	double m_deadlineMetricWeight;
	double m_requestDeniedSpread;
	double m_congestionLow;
	double m_congestionHigh;
	double m_congestion;
	double m_congestionTimeWindow;
	double m_minPercentRBsM2MLow;
	double m_minPercentRBsM2MNormal;
	double m_minPercentRBsM2MHigh;
};

#endif /* UP_M2M_PACKET_SCHEDULER_H_ */
