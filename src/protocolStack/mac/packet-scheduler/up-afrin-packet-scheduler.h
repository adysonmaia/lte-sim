/*
 * up-afrin-packet-scheduler.h
 *
 *  Created on: 13/05/2014
 *      Author: adyson
 */

#ifndef UP_AFRIN_PACKET_SCHEDULER_H_
#define UP_AFRIN_PACKET_SCHEDULER_H_

#include "up-abdalla-packet-scheduler.h"
#include "up-pf-packet-scheduler.h"

class UP_Afrin_PacketScheduler: public UP_Abdalla_PacketScheduler {
public:
	UP_Afrin_PacketScheduler();
	virtual ~UP_Afrin_PacketScheduler();
protected:
	using UP_PF_PacketScheduler::ComputeSchedulingMetric;
	virtual double ComputeSchedulingMetric(UserToSchedule *user);
	virtual int DoH2HSchedule(UsersToSchedule *users, int rbStart, int nbRBs);
	virtual int DoM2MSchedule(UsersToSchedule *users, int rbStart, int nbRBs);

	int m_maxDataToTransmit;
};

#endif /* UP_AFRIN_PACKET_SCHEDULER_H_ */
