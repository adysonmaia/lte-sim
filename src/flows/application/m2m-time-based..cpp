/*
 * m2m-time-based..cpp
 *
 *  Created on: 10/05/2014
 *      Author: adyson
 */

#include "m2m-time-based.h"
#include <cstdlib>
#include "../../componentManagers/NetworkManager.h"
#include "../radio-bearer.h"

M2M_TimeBased::M2M_TimeBased() :
		m_size(0), m_interval(0) {
	SetApplicationType(Application::APPLICATION_TYPE_M2M_TIME_BASED);
}

M2M_TimeBased::~M2M_TimeBased() {
	Destroy();
}

void M2M_TimeBased::DoStart(void) {
	Simulator::Init()->Schedule(0.0, &M2M_TimeBased::Send, this);
}

void M2M_TimeBased::DoStop(void) {
}

void M2M_TimeBased::ScheduleTransmit(double time) {
	if ((Simulator::Init()->Now() + time) < GetStopTime()) {
		Simulator::Init()->Schedule(time, &M2M_TimeBased::Send, this);
	}
}

void M2M_TimeBased::Send(void) {
	//CREATE A NEW PACKET (ADDING UDP, IP and PDCP HEADERS)
	Packet *packet = new Packet();
	int uid = Simulator::Init()->GetUID();

	packet->SetID(uid);
	packet->SetTimeStamp(Simulator::Init()->Now());
	packet->SetSize(GetSize());

	PacketTAGs *tags = new PacketTAGs();
	tags->SetApplicationType(PacketTAGs::APPLICATION_TYPE_M2M_TIME_BASED);
	tags->SetApplicationSize(packet->GetSize());
	packet->SetPacketTags(tags);

	UDPHeader *udp = new UDPHeader(GetClassifierParameters()->GetSourcePort(),
			GetClassifierParameters()->GetDestinationPort());
	packet->AddUDPHeader(udp);

	IPHeader *ip = new IPHeader(GetClassifierParameters()->GetSourceID(),
			GetClassifierParameters()->GetDestinationID());
	packet->AddIPHeader(ip);

	PDCPHeader *pdcp = new PDCPHeader();
	packet->AddPDCPHeader(pdcp);

	Trace(packet);

	GetRadioBearer()->Enqueue(packet);

	ScheduleTransmit(GetInterval());

}

int M2M_TimeBased::GetSize(void) const {
	return m_size;
}

void M2M_TimeBased::SetSize(int size) {
	m_size = size;
}
void M2M_TimeBased::SetInterval(double interval) {
	m_interval = interval;
}

double M2M_TimeBased::GetInterval(void) const {
	return m_interval;
}

