/*
 * m2m-time-based.h
 *
 *  Created on: 10/05/2014
 *      Author: adyson
 */

#ifndef M2M_TIME_BASED_H_
#define M2M_TIME_BASED_H_

#include "Application.h"

class M2M_TimeBased: public Application {
public:
	M2M_TimeBased();
	virtual ~M2M_TimeBased();

	virtual void DoStart(void);
	virtual void DoStop(void);

	void ScheduleTransmit(double time);
	void Send(void);

	void SetSize(int size);
	int GetSize(void) const;
	void SetInterval(double interval);
	double GetInterval(void) const;

private:
	double m_interval;
	int m_size;
};

#endif /* M2M_TIME_BASED_H_ */
