/*
 * m2m-event-based.cpp
 *
 *  Created on: 10/05/2014
 *      Author: adyson
 */

#include "m2m-event-based.h"
#include <cstdlib>
#include "../../componentManagers/NetworkManager.h"
#include "../radio-bearer.h"

M2M_EventBased::M2M_EventBased() :
		m_mean(1.0), m_size(0) {
	SetApplicationType(Application::APPLICATION_TYPE_M2M_EVENT_BASED);
}

M2M_EventBased::~M2M_EventBased() {
	Destroy();
}

void M2M_EventBased::DoStart(void) {
	Simulator::Init()->Schedule(0.0, &M2M_EventBased::Send, this);
}

void M2M_EventBased::DoStop(void) {
}

double M2M_EventBased::GetNextInterval() {
	// TODO change exponencial to poisson
	double f = (double) rand() / RAND_MAX;
	f = -log(1 - f) / (1.0 / m_mean);
	return f;
}

void M2M_EventBased::ScheduleTransmit(double time) {
	if ((Simulator::Init()->Now() + time) < GetStopTime()) {
		Simulator::Init()->Schedule(time, &M2M_EventBased::Send, this);
	}
}

void M2M_EventBased::Send(void) {
	//CREATE A NEW PACKET (ADDING UDP, IP and PDCP HEADERS)
	Packet *packet = new Packet();
	int uid = Simulator::Init()->GetUID();

	packet->SetID(uid);
	packet->SetTimeStamp(Simulator::Init()->Now());
	packet->SetSize(GetSize());

	PacketTAGs *tags = new PacketTAGs();
	tags->SetApplicationType(PacketTAGs::APPLICATION_TYPE_M2M_EVENT_BASED);
	tags->SetApplicationSize(packet->GetSize());
	packet->SetPacketTags(tags);

	UDPHeader *udp = new UDPHeader(GetClassifierParameters()->GetSourcePort(),
			GetClassifierParameters()->GetDestinationPort());
	packet->AddUDPHeader(udp);

	IPHeader *ip = new IPHeader(GetClassifierParameters()->GetSourceID(),
			GetClassifierParameters()->GetDestinationID());
	packet->AddIPHeader(ip);

	PDCPHeader *pdcp = new PDCPHeader();
	packet->AddPDCPHeader(pdcp);

	Trace(packet);

	GetRadioBearer()->Enqueue(packet);

	ScheduleTransmit(GetNextInterval());
}

