/*
 * m2m-event-based.h
 *
 *  Created on: 10/05/2014
 *      Author: adyson
 */

#ifndef M2M_EVENT_BASED_H_
#define M2M_EVENT_BASED_H_

#include "Application.h"

class M2M_EventBased: public Application {
public:
	M2M_EventBased();
	virtual ~M2M_EventBased();

	virtual void DoStart(void);
	virtual void DoStop(void);

	void ScheduleTransmit(double time);
	void Send(void);

	inline void SetSize(int size) {this->m_size = size;}
	inline int GetSize(void) const {return this->m_size;}

	inline double GetMean() const {return this->m_mean;}
	inline void SetMean(double mean) {this->m_mean = mean;}

	double GetNextInterval();

private:
	double m_mean;
	int m_size;
};

#endif /* M2M_EVENT_BASED_H_ */
