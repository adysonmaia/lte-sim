/*
 * m2m.h
 *
 *  Created on: 08/05/2014
 *      Author: adyson
 */

#ifndef M2M_DEADLINE_WEIGHT_H_
#define M2M_DEADLINE_WEIGHT_H_

#include "../channel/LteChannel.h"
#include "../core/spectrum/bandwidth-manager.h"
#include "../networkTopology/Cell.h"
#include "../core/eventScheduler/simulator.h"
#include "../flows/application/Application.h"
#include "../flows/application/InfiniteBuffer.h"
#include "../flows/application/VoIP.h"
#include "../flows/application/CBR.h"
#include "../flows/application/WEB.h"
#include "../flows/application/TraceBased.h"
#include "../flows/application/m2m-time-based.h"
#include "../flows/application/m2m-event-based.h"
#include "../flows/QoS/QoSParameters.h"
#include "../componentManagers/FrameManager.h"
#include "../componentManagers/FlowsManager.h"
#include "../device/ENodeB.h"
#include "../utility/seed.h"
#include "../utility/RandomVariable.h"
#include "../utility/ConfigParameters.h"
#include "../load-parameters.h"
#include <iostream>
#include <queue>
#include <fstream>
#include <stdlib.h>
#include <string>
#include <cstring>
#include <vector>
#include <exception>

#define QCI_VOIP 1
#define QCI_VIDEO 2
#define QCI_CBR 8
#define QCI_M2M_EVENT 10
#define QCI_M2M_TIME 11
#define MAX_DELAY_VOIP 0.1 // s
#define MAX_DELAY_VIDEO 0.15
#define MAX_DELAY_CBR 0.3
#define MAX_DELAY_M2M_EVENT 0.05
#define CBR_INTERVAL 0.015625
#define CBR_SIZE 256 // bytes
#define M2M_SIZE 125
#define M2M_EVENT_LAMBDA 0.05 // s

#define SIM_DURATION 5.0 // s
#define CELL_RADIUS 1 // km
#define CELL_MIN_DISTANCE 0.05 // km
#define BANDWIDTH 10 // MHz
#define UE_SPEED 3.0 // km/h

static void M2MDeadlineWeight(double deadlineWeight, int seed) {
	int nbVoIP = 30;
	int nbVideo  = 30;
	int nbCBR = 30;
	int nbM2MTime = 700;
	int nbM2MEvent = 300;
	double maxRBsForM2M = 0.4;
	int rbM2M = 4;
	ENodeB::ULSchedulerType upSchedulerType = ENodeB::ULScheduler_TYPE_M2M_V1;

	// CREATE COMPONENT MANAGER
	Simulator *simulator = Simulator::Init();
	FrameManager *frameManager = FrameManager::Init();
	NetworkManager* networkManager = NetworkManager::Init();
	FlowsManager* flowsManager = FlowsManager::Init();
	ConfigParameters* config = ConfigParameters::Init();

	config->AddValue(CONF_PARAM_PERCENT_RB_M2M, maxRBsForM2M);
	config->AddValue(CONF_PARAM_DEADLINE_WEIGHT, deadlineWeight);
	config->AddValue(CONF_PARAM_RB_M2M, rbM2M);
	config->AddValue(CONF_PARAM_RB_H2H, rbM2M);

	// CONFIGURE SEED
	if (seed >= 0 && seed < 9) {
		// TODO common seeds for 30 runs
		int commonSeed = GetCommonSeed(seed);
		srand(commonSeed);
	} else {
		srand(time(NULL));
	}

	frameManager->SetFrameStructure(FrameManager::FRAME_STRUCTURE_FDD);

// CREATE CELL
	Cell *cell = new Cell(0, CELL_RADIUS, CELL_MIN_DISTANCE, 0, 0);
	networkManager->GetCellContainer()->push_back(cell);

	// CREATE CHANNELS and propagation loss model
	LteChannel *dlCh = new LteChannel();
	LteChannel *ulCh = new LteChannel();

	// CREATE SPECTRUM
	BandwidthManager* spectrum = new BandwidthManager(BANDWIDTH, BANDWIDTH, 0, 0);

	//Create ENodeB
	ENodeB* enb = new ENodeB(1, cell, 0, 0);
	enb->GetPhy()->SetDlChannel(dlCh);
	enb->GetPhy()->SetUlChannel(ulCh);
	enb->GetPhy()->SetBandwidthManager(spectrum);
	ulCh->AddDevice(enb);
	enb->SetULScheduler(upSchedulerType);
	enb->SetDLScheduler(ENodeB::DLScheduler_TYPE_PROPORTIONAL_FAIR);
	networkManager->GetENodeBContainer()->push_back(enb);

	//Create GW
	Gateway *gw = new Gateway();
	networkManager->GetGatewayContainer()->push_back(gw);

	std::vector<Application*> applications;

	int idUE = 2;
	int destinationPort = 101;
	int applicationID = 0;
	double flowDuration = SIM_DURATION;

	std::map<Application::ApplicationType, int> nbUEsPerApp;
	std::map<Application::ApplicationType, int>::iterator itUE;
	nbUEsPerApp.insert(std::pair<Application::ApplicationType, int>(Application::APPLICATION_TYPE_VOIP, nbVoIP));
	nbUEsPerApp.insert(
			std::pair<Application::ApplicationType, int>(Application::APPLICATION_TYPE_TRACE_BASED, nbVideo));
	nbUEsPerApp.insert(std::pair<Application::ApplicationType, int>(Application::APPLICATION_TYPE_CBR, nbCBR));
	nbUEsPerApp.insert(
			std::pair<Application::ApplicationType, int>(Application::APPLICATION_TYPE_M2M_TIME_BASED, nbM2MTime));
	nbUEsPerApp.insert(
			std::pair<Application::ApplicationType, int>(Application::APPLICATION_TYPE_M2M_EVENT_BASED, nbM2MEvent));

	//Create UEs
	for (itUE = nbUEsPerApp.begin(); itUE != nbUEsPerApp.end(); itUE++) {
		for (int i = 0; i < itUE->second; i++) {
			//ue's random position
			double posX = (double) rand() / RAND_MAX;
			posX = 0.95 * (((2 * CELL_RADIUS * 1000) * posX) - (CELL_RADIUS * 1000));
			double posY = (double) rand() / RAND_MAX;
			posY = 0.95 * (((2 * CELL_RADIUS * 1000) * posY) - (CELL_RADIUS * 1000));
			double speedDirection = (double) (rand() % 360) * ((2 * 3.14) / 360);

			UserEquipment* ue;
			Mobility::MobilityModel model = Mobility::RANDOM_WALK;
			if (itUE->first == Application::APPLICATION_TYPE_M2M_TIME_BASED
					|| itUE->first != Application::APPLICATION_TYPE_M2M_EVENT_BASED) {
				model = Mobility::CONSTANT_POSITION;
			}
			ue = new UserEquipment(idUE, posX, posY, UE_SPEED, speedDirection, cell, enb, false, model);

			ue->GetPhy()->SetDlChannel(dlCh);
			ue->GetPhy()->SetUlChannel(ulCh);

			FullbandCqiManager *cqiManager = new FullbandCqiManager();
			cqiManager->SetCqiReportingMode(CqiManager::PERIODIC);
			cqiManager->SetReportingInterval(1);
			cqiManager->SetDevice(ue);
			ue->SetCqiManager(cqiManager);

			WidebandCqiEesmErrorModel *errorModel = new WidebandCqiEesmErrorModel();
			ue->GetPhy()->SetErrorModel(errorModel);

			networkManager->GetUserEquipmentContainer()->push_back(ue);

			// register ue to the enb
			enb->RegisterUserEquipment(ue);
			ue->SetTargetNode(enb);
			// define the channel realization
			MacroCellUrbanAreaChannelRealization* c_dl = new MacroCellUrbanAreaChannelRealization(enb, ue);
			enb->GetPhy()->GetDlChannel()->GetPropagationLossModel()->AddChannelRealization(c_dl);
			MacroCellUrbanAreaChannelRealization* c_ul = new MacroCellUrbanAreaChannelRealization(ue, enb);
			enb->GetPhy()->GetUlChannel()->GetPropagationLossModel()->AddChannelRealization(c_ul);

			// create application
			double startTime = (rand() % 50) / 1000.0; // 50 ms
			QoSParameters *qos = new QoSParameters();
			Application* app = flowsManager->CreateApplication(applicationID, ue, enb, 0, destinationPort,
					TransportProtocol::TRANSPORT_PROTOCOL_TYPE_UDP, itUE->first, qos, startTime, flowDuration);
			applications.push_back(app);

			if (itUE->first == Application::APPLICATION_TYPE_VOIP) {
				qos->SetQCI(QCI_VOIP);
				qos->SetMaxDelay(MAX_DELAY_VOIP);
			} else if (itUE->first == Application::APPLICATION_TYPE_TRACE_BASED) {
				qos->SetQCI(QCI_VIDEO);
				qos->SetMaxDelay(MAX_DELAY_VIDEO);
				std::string filePath = path + "src/flows/application/Trace/mobile_H264_128k.dat";
				static_cast<TraceBased*>(app)->SetTraceFile(filePath);
			} else if (itUE->first == Application::APPLICATION_TYPE_CBR) {
				qos->SetQCI(QCI_CBR);
				qos->SetMaxDelay(MAX_DELAY_CBR);
				static_cast<CBR*>(app)->SetInterval(CBR_INTERVAL);
				static_cast<CBR*>(app)->SetSize(CBR_SIZE);
			} else if (itUE->first == Application::APPLICATION_TYPE_M2M_TIME_BASED) {
//				double minInterval = 0.05;
				double minInterval = MAX_DELAY_M2M_EVENT;
				double maxInterval = flowDuration;
				double interval = minInterval + ((double) rand() / RAND_MAX) * (maxInterval - minInterval);
				qos->SetQCI(QCI_M2M_TIME);
				qos->SetMaxDelay(interval); // TODO use classes?
				static_cast<M2M_TimeBased*>(app)->SetInterval(interval);
				static_cast<M2M_TimeBased*>(app)->SetSize(M2M_SIZE);
			} else if (itUE->first == Application::APPLICATION_TYPE_M2M_EVENT_BASED) {
				qos->SetQCI(QCI_M2M_EVENT);
				qos->SetMaxDelay(MAX_DELAY_M2M_EVENT);
				static_cast<M2M_EventBased*>(app)->SetMean(M2M_EVENT_LAMBDA);
				static_cast<M2M_EventBased*>(app)->SetSize(M2M_SIZE);
			}

			//update counter
			idUE++;
			destinationPort++;
			applicationID++;
		}
	}

	simulator->SetStop(flowDuration + 0.5);
	simulator->Run();
}

#endif /* M2M_DEADLINE_WEIGHT_H_ */
