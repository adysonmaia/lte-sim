#include "ConfigParameters.h"
#include <iostream>
#include <utility>

ConfigParameters *ConfigParameters::ptr = NULL;

ConfigParameters::ConfigParameters() :
		m_maxRBsForM2M(-1.0) {
}

ConfigParameters::~ConfigParameters() {
	m_values.clear();
}

ConfigParameters* ConfigParameters::Init(void) {
	if (ptr == NULL) {
		ptr = new ConfigParameters();
	}
	return ptr;
}

void ConfigParameters::AddValue(const std::string &key, int &value) {
	m_values.insert(std::make_pair(key, &value));
}

void ConfigParameters::AddValue(const std::string &key, double &value) {
	m_values.insert(std::make_pair(key, &value));
}

int ConfigParameters::GetIntValue(const std::string &key, int defaultValue) {
	void *value_ptr = GetValue(key);
	if (value_ptr == NULL)
		return defaultValue;
	else
		return *((int *) value_ptr);
}

double ConfigParameters::GetDoubleValue(const std::string &key, double defaultValue) {
	void *value_ptr = GetValue(key);
	if (value_ptr == NULL)
		return defaultValue;
	else
		return *((double *) value_ptr);
}

void* ConfigParameters::GetValue(const std::string &key) {
	std::map<std::string, void*>::iterator it = m_values.find(key);
	if (it != m_values.end())
		return it->second;
	else
		return NULL;
}
