/*
 * Config.h
 *
 *  Created on: 14/05/2014
 *      Author: adyson
 */

#ifndef CONFIG_PARAMETERS_H_
#define CONFIG_PARAMETERS_H_

#include <map>
#include <string>

#define CONF_PARAM_PERCENT_RB_M2M "minPercentRBsM2M"
#define CONF_PARAM_RB_H2H "rbsH2H"
#define CONF_PARAM_RB_M2M "rbsM2M"
#define CONF_PARAM_REQUEST_SPREAD "requestDeniedSpread"
#define CONF_PARAM_DEADLINE_WEIGHT "deadlineMetricWeight"
#define CONF_PARAM_CONGESTION_TW "congestionTimeWindow"
#define CONF_PARAM_PERCENT_RB_LOW "minPercentRBsM2MLow"
#define CONF_PARAM_PERCENT_RB_NORMAL "minPercentRBsM2MNormal"
#define CONF_PARAM_PERCENT_RB_HIGH "minPercentRBsM2MHigh"
#define CONF_PARAM_CONGESTION_LOW "congestionLow"
#define CONF_PARAM_CONGESTION_HIGH "congestionHigh"

class ConfigParameters {
public:
	ConfigParameters();
	virtual ~ConfigParameters();
	static ConfigParameters* Init(void);

	void AddValue(const std::string &key, int &value);
	void AddValue(const std::string &key, double &value);
	int GetIntValue(const std::string &key, int defaultValue);
	double GetDoubleValue(const std::string &key, double defaultValue);
	void *GetValue(const std::string &key);

	inline void SetMaxRBsForM2M(double value) {
		this->m_maxRBsForM2M = value;
	}

	inline double GetMaxRBsForM2M() const {
		return this->m_maxRBsForM2M;
	}
private:
	static ConfigParameters *ptr;
	double m_maxRBsForM2M;

	std::map<std::string, void *> m_values;
};

#endif /* CONFIG_PARAMETERS_H_ */
